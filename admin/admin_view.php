<?php
    session_start();
    if(!isset($_SESSION['username']) || $_SESSION['accountType'] !== 'admin'){
        header("location: ../index.php");
        die();
    }
    include "../van/php/sql-statements.php";

    $db = new DB();

    $entries = $db->getRows('tbl_entries');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title id="paymentTitleNotificaiton">BIDA - Admin Panel</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin_view.php">BIDA - Admin Menu</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li>
                    <a href="change_pass.php" id="changePass"><i class="fa fa-fw fa-gear"></i> Change Password</a>
                </li>
                <li>
                    <a href="../index.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="admin_view.php"><i class="glyphicon glyphicon-search"></i> View Entries <span class="badge" id="paymentNotificaiton">0</span></a>
                    </li>
<!--                    <li>-->
<!--                        <a href="admin_validate.php"><i class="glyphicon glyphicon-ok"></i> Validate Payment </a>-->
<!--                    </li>-->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            View <small>Entries</small>
                        </h1>
                    </div>
                </div>

                <div class="form-group">
                    <form id="admin_view">
                        <select id="college" name="college" class="form-control">
                        <option>SELECT SCHOOL</option>
                        <?php
                        $schools = array();
                            for($i = 0; $i < count($entries); $i++){
                                $countEntry = $db->getRows('tbl_entries', array('where'=>array('college'=>$entries[$i]['college'], 'entry_status'=>'pending'), 'return_type'=>'count'));
                                if(!in_array($entries[$i]['college'], $schools)){
                                    if($countEntry > 0){
                                        echo "<option style='background: #ff8872;'>" .$entries[$i]['college']. "</option>";
                                    }else{
                                        echo "<option>" .$entries[$i]['college']. "</option>";
                                    }
                                    array_push($schools, $entries[$i]['college']);
                                }
                            }
                        ?>
                        </select>
                    </form>
                </div>

                <div class="table">
                    <table class="table table-bordered table-hover table-striped" style="float: left; width: 100%;">
                        <thead>
                            <tr>
                                <th>Food</th>
                            </tr>
                        </thead>
                        <tbody id="Food">
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-hover table-striped" style="float: left; width: 100%;">
                        <thead>
                            <tr>
                                <th>Non-Food</th>
                            </tr>
                        </thead>
                        <tbody  id="Non-Food">
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-hover table-striped" style="float: left; width: 100%;">
                        <thead>
                            <tr>
                                <th>Service</th>
                            </tr>
                        </thead>
                        <tbody id="Services">
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-hover table-striped" style="float: left; width: 100%;">
                        <thead>
                            <tr>
                                <th>Technology</th>
                            </tr>
                        </thead>
                        <tbody id="Technology">
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
        <div id="myModal" class="modal fade" role="dialog">
        <form id="modalForm">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modalHeader">ENTRY INFORMATION <span id="verifyStatus"></span></h4>
              </div>
              <div class="modal-body" id="details">
                <h2 style="text-align: center;"></h2>
                  <h5 style="text-align: center;"></h5>
                  <br>
                  <p style="text-align: justify; margin: 0 25%;"></p>
              </div>
              <div class="modal-footer" id="modalButtons">
                  <input type="checkbox" id="verifyEntry" data-width="100" data-toggle="toggle" data-on="Approved" data-off="Pending" data-onstyle="success" data-offstyle="warning">
                  <div class="checkbox" style="display: inline!important;">
                      <label>
                          <input type="checkbox" id="verifyPayment"> Verify Payment
                      </label>
                  </div>
                <button type="button" id="saveEntry" class="btn btn-default">Save</button>
              </div>
            </div>
          </div>
          </form>
        </div>
    </div>


    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/admin_view.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>