<?php
    include "../../van/php/sql-statements.php";
    include "../../van/php/mailConfig.php";
    session_start();

    $db = new DB();

    $id = $_POST['id'];
    $type = $_POST['type'];

    $entries = $db->getRows('tbl_entries', array('where'=>array('entry_id'=>$id)));
    $user = $db->getRows('tbl_accounts', array('where'=>array('account_id'=>$entries[0]['account_id'])));

    $conditions = array(
        'entry_id'=>$id
    );

    $mail->addAddress($user[0]['personal_email'], $user[0]['first_name']);
    $mail->isHTML(true);

    if($type === 'entry_status'){
        if($_POST['entryVerify'] == 'false') {
            $data = array(
                $type => 'pending'
            );
        } else {
            $data = array(
                $type => 'verified'
            );
            $mail->Subject = 'Your entry has been verified';
            $mail->Body = '<p>Greetings!</p><p>Your entry entitled "' . $entries[0]["title"] . '" has now been verified. For questions or inquiries you may contact us through the contact us form.</p><p>Regards,<br>BIDA Administrator</p>';
            if (!$mail->send()) {
                echo 'Mailer Error: ' . $mail->ErrorInfo;
                die();
            }
        }
    }else{
        $data = array(
            $type=>'verified'
        );
        $mail->Subject = 'Your payment has been verified';
        $mail->Body    = '<p>Greetings!</p><p>Your payment has been verified. For questions or inquiries you may contact us through the contact us form.</p><p>Regards,<br>BIDA Administrator</p></p>';
        if(!$mail->send()) {
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            die();
        }
    }

    $sql = $db->update('tbl_entries', $data, $conditions);

    if(!is_array($sql) && $sql > 0){
        echo "success";
    }else{
        if(is_array($sql)){
            print_r($sql);
        }else if($type === 'entry_status'){
            echo "success";
        }else{
            echo "SQL: " . $sql;
        }
    }
?>