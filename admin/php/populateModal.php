<?php
    include "../../van/php/sql-statements.php";

    $db = new DB();

    $id = $_POST['id'];

    $conditions = array(
        'where'=>array(
            'entry_id'=>$id
        )
    );
    $sql = $db->getRows('tbl_entries', $conditions);

    if(!$sql){
        echo 'fail';
    }else{
        echo json_encode($sql);
    }
?>