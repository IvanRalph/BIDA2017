<?php
    include "../../van/php/sql-statements.php";

    $db = new DB();

    $category = $_POST['category'];
    $college = $_POST['college'];

    $conditions = array(
        'where'=>array(
            'category'=>$category,
            'college'=>$college
        )
    );

    $returnData = $db->getRows('tbl_entries', $conditions);

    if(!$returnData){
        echo json_encode(array('status'=>'fail'));
    }else{
        echo json_encode($returnData);
    }
?>