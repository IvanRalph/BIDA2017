<?php
    session_start();
    if(!isset($_SESSION['username']) || $_SESSION['accountType'] !== 'admin'){
        header("location: ../index.php");
        die();
    }
    include "../van/php/sql-statements.php";

    $db = new DB();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title id="paymentTitleNotificaiton">BIDA - Change Password</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="admin_view.php">BIDA - Admin Menu</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li>
                <a href="#" id="changePass"><i class="fa fa-fw fa-gear"></i> Change Password</a>
            </li>
            <li>
                <a href="../index.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active">
                    <a href="admin_view.php"><i class="glyphicon glyphicon-search"></i> View Entries <span class="badge" id="paymentNotificaiton">0</span></a>
                </li>
                <!--                    <li>-->
                <!--                        <a href="admin_validate.php"><i class="glyphicon glyphicon-ok"></i> Validate Payment </a>-->
                <!--                    </li>-->
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <?php
        $user = $db->getRows('tbl_accounts', array('where'=>array('username'=>$_SESSION['username'])));
    ?>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Account <small>Settings</small>
                </h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <form class="form-horizontal" id="accountSettings" style="border-right: 1px solid #d6d6d6;">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Username</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $user[0]['username']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">School Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="schoolName" placeholder="School Name" value="<?php echo $user[0]['school_name']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">School Address</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="schoolAddress" placeholder="School Address" value="<?php echo $user[0]['school_address']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">School Contact No.</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="schoolNumber" placeholder="School Contact No." value="<?php echo $user[0]['contact_info']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">School E-Mail</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="schoolEmail" placeholder="School E-Mail" value="<?php echo $user[0]['school_email']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">First Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="firstName" placeholder="First Name" value="<?php echo $user[0]['first_name']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Last Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="lastName" placeholder="Last Name" value="<?php echo $user[0]['last_name']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-lg-6">
                    <form class="form-horizontal" id="changePassForm2">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Old Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="oldPassword" placeholder="Old Password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">New Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="newPassword" placeholder="New Password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Confirm New Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="conNewPassword" placeholder="Confirm New Password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
</div>


<!-- /#wrapper -->

<!-- jQuery -->
<script src="js/jquery.js"></script>
<script src="js/admin_view.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="js/plugins/morris/raphael.min.js"></script>
<script src="js/plugins/morris/morris.min.js"></script>
<script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>