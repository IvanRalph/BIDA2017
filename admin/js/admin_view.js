var id;

$("#college").on('change', function(){
    $("#Food tr td, #Non-Food tr td, #Services tr td, #Technology tr td").html("");
    $("#Food tr td, #Non-Food tr td, #Services tr td, #Technology tr td").css("background", "none");
    var college = $(this).val();
    var category = [
        'Food',
        'Non-Food',
        'Services',
        'Technology'
    ];
    for(var i = 0; i < category.length; i++){
        populateTables(category[i], college);
    }
});

function populateTables(category, college){
    $.ajax({
        url: 'php/populateData.php',
        type: 'POST',
        data: 'category=' + category + "&college=" + college,
        dataType: 'JSON',
        success: function(data, result){
            if(result !== "success"){
                console.log("RESULT FAILED: " + data);
            }else{
                if(data.status === 'fail'){
                    console.log(data.status);
                }else{
                    for(var i = 0; i < data.length; i++) {
                        $("#" + category + " tr td").append("<a href='' class='viewEntry' data-toggle='modal' data-target='#myModal' id='"+ data[i].entry_id +"'>"+ data[i].title +"</a>");
                        $("#" + category + " tr td").append("<button type='button' onclick='downloadFile(\"" + data[i].attachDocu + "\")' style='float: right;' class='btn btn-primary'>Document</button>");
                        $("#" + category + " tr td").append("<button type='button' onclick='downloadFile(\"" + data[i].attachPresentation + "\")' style='float: right;' class='btn btn-primary'>Presentation</button>");
                        if(data[i].attachPayment !== ""){
                            $("#" + category + " tr td").append("<button type='button' onclick='downloadFile(\"" + data[i].attachPayment + "\")' style='float: right;' class='btn btn-primary'>Payment</button>");
                        }
                        if(data[i].entry_status === 'verified'){
                            $("#" + category + " tr td").css('background', '#1abc9c');
                            $('#verifyEntry').bootstrapToggle('on');
                        }else{
                            $("#" + category + " tr td").css('background', '#ff8872');
                            $('#verifyEntry').bootstrapToggle('off');
                        }
                        if(data[i].status === 'verified'){
                            $("#verifyStatus").html('<span class="label label-success">Payment Verified</span>');
                            $("#modalButtons .checkbox").css('display','none');
                        }else{
                            $("#verifyStatus").html('<span class="label label-danger">Payment not verified</span>');
                            $("#modalButtons .checkbox").css('display','inline');
                        }
                    }
                }
            }
        },
        error: function(xhr, status, thrown){
            console.log("ERROR: " + status + " THROW: " + thrown + " XHR: " + xhr.responseText);
        }
    });
}

$(document).on('click', '#saveEntry', function(e){
    e.preventDefault();
    $(this).html("Loading...");
    $(this).attr('disabled', 'disabled');
    var con = confirm("Are you sure you want to save changes?");

    if(con){
        if($("#verifyPayment").is(':checked')){
            ajaxVerified('status');
        }
        ajaxVerified('entry_status');

        function ajaxVerified(type){
            var formData;
            if(type === 'entry_status'){
                formData = "id=" + id + "&type=" + type + "&entryVerify=" + $("#verifyEntry").prop('checked');
            }else{
                formData = "id=" + id + "&type=" + type;
            }
            $.ajax({
                url: 'php/updateVerified.php',
                type: 'POST',
                data: formData,
                success: function(data, status){
                    if(status === "success"){
                        if(data === "success"){
                            if(type === 'entry_status'){
                                location.reload();
                            }
                        }else{
                            alert("Internal Server Error");
                            console.log(data);
                        }
                    }else{
                        console.log("RESULT FAILED: " + data);
                    }
                },
                error: function(xhr, status, thrown){
                    console.log("ERROR: " + status + " THROW: " + thrown + " XHR: " + xhr.responseText);
                }
            });
        }
    }
});

$(document).on('click', '.viewEntry',function(e){
    e.preventDefault();
    id = $(this).attr('id');
    $.ajax({
        url: 'php/populateModal.php',
        type: 'POST',
        data: 'id=' + id,
        dataType: 'JSON',
        success: function(data, result){
            if(result !== "success"){
                console.log("RESULT FAILED: " + data);
            }else{
                if(data.status === 'fail'){
                    console.log(data.status);
                }else{
                    $("#receiptPicture").remove();
                    $("#details h2").html(data[0].title);
                    $("#details p").html(data[0].description);
                    $("#details h5").html("by " + data[0].college);
                }
            }
        },
        error: function(xhr, status, thrown){
            console.log("ERROR: " + status + " THROW: " + thrown + " XHR: " + xhr.responseText);
        }
    });
});

$("#changePassForm").on('submit', function (e) {
   e.preventDefault();
   $.ajax({
       url: "../van/php/changePassword.php",
       type: "POST",
       data: $(this).serialize(),
       success: function(data, result){
           if(result !== "success"){
               console.log("RESULT FAILED: " + data);
           }else{
               if(data === 'success'){
                   alert("Password changed successfully!");
                   window.location.href = "admin_view.php";
               }else{
                   alert(data);
               }
           }
       },
       error: function(xhr, status, thrown){
           console.log("ERROR: " + status + " THROW: " + thrown + " XHR: " + xhr.responseText);
       }
   });
});

$("#accountSettings").on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        url: "../van/php/updateProfile.php",
        type: "POST",
        data: $(this).serialize(),
        success: function(data, result){
            if(result !== "success"){
                console.log("RESULT FAILED: " + data);
            }else{
                if(data === 'success'){
                    alert("Account Updated successfully!");
                    window.location.href = "admin_view.php";
                }else{
                    alert(data);
                }
            }
        },
        error: function(xhr, status, thrown){
            console.log("ERROR: " + status + " THROW: " + thrown + " XHR: " + xhr.responseText);
        }
    });
});

function downloadFile(file){
    window.location.href = "../uploads/" + file;
}

setInterval(function(){
    $.ajax({
        url: "php/admin_notification.php",
        success: function(data, result){
            if(result === "fail"){
                console.log(data);
            }else{
                $("#paymentNotificaiton").html(data);
                if(data > 0){
                    $("#paymentTitleNotificaiton").html("("+data+") BIDA - Admin Payment Validation");
                }else{
                    $("#paymentTitleNotificaiton").html("BIDA - Admin Payment Validation");
                }
            }
        },
        error: function(xhr, status, thrown){
            console.log("ERROR: " + status + " THROW: " + thrown + " XHR: " + xhr.responseText);
        }
    });
}, 500);