<?php
    session_start();
    include "sql-statements.php";

    $db = new DB();

    $oldPass = $_POST['oldPassword'];
    $newPass = $_POST['newPassword'];
    $conNewPass = $_POST['conNewPassword'];

    if($newPass !== $conNewPass){
        echo "New Password does not match!";
        die();
    }

    $user = $db->getRows('tbl_accounts', array('where'=>array('username'=>$_SESSION['username'])));

    if(md5(sha1($oldPass)) !== $user[0]['password']){
        echo "Old password is incorrect!";
        die();
    }

    $data = array(
        'password'=>md5(sha1($newPass))
    );

    $condition = array(
        'account_id'=>$user[0]['account_id']
    );

    $updatePassword = $db->update('tbl_accounts', $data, $condition);

    if($updatePassword != false){
        echo "success";
    }
?>