a<?php
	include "sql-statements.php";
	// include "dbconfig.php";

	// if(!$conn){
	// 	echo "DB Error";
	// 	die();
	// }

	$db = new DB();

	$username = $_POST['username'];
	$password = md5(sha1($_POST['password']));
	$schoolName = $_POST['schoolName'];
	$schoolAddress = $_POST['schoolAddress'];
	$contactInfo = $_POST['contactInfo'];
	$schoolEmail = $_POST['schoolEmail'];
	$lastName = $_POST['lastName'];
	$firstName = $_POST['firstName'];
	if(!$_POST['middleName']){
		$middleName = "";
	}else{
		$middleName = $_POST['middleName'];
	}
	$gender = $_POST['gender'];
	$birthDate = $_POST['dob'];
	$personalEmail = $_POST['personalEmail'];
	$contactNumber = $_POST['contactNumber'];
	$department = $_POST['department'];
	$position = $_POST['position'];
	$schoolHead = $_POST['schoolHead'];

	$users = $db->getRows('tbl_accounts', array('where'=>array('username'=>$username), 'return_type'=>'count'));

	if($users > 0){
		echo "user exists";
		die();
	}
	$userData = array(
		'username'=>$username,
		'password'=>$password,
		'school_name'=>$schoolName,
		'school_address'=>$schoolAddress,
		'contact_info'=>$contactInfo,
		'school_email'=>$schoolEmail,
		'last_name'=>$lastName,
		'first_name'=>$firstName,
		'middle_name'=>$middleName,
		'gender'=>$gender,
		'birth_date'=>$birthDate,
		'personal_email'=>$personalEmail,
		'contact_number'=>$contactNumber,
		'department'=>$department,
		'position'=>$position,
		'school_head'=>$schoolHead,
		'account_type'=>'standard'
	);

	$registerUser = $db->insert('tbl_accounts', $userData);

	if(!$registerUser){
		echo "fail";
	}else{
		echo "success";
	}
?>