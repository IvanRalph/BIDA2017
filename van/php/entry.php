<?php
	include "sql-statements.php";
	session_start();

	$db = new DB();

	$category = $_POST['category'];
	$title = $_POST['title'];
	$description = $_POST['description'];
	$college = $_POST['college'];
	$collegeDean = $_POST['collegeDean'];
	$collegeAddress = $_POST['collegeAddress'];
	$coach = $_POST['coach'];
	$contactPerson = $_POST['contactPerson'];
	$landline = $_POST['landline'];
	$mobile = $_POST['mobile'];
	$email = $_POST['email'];
	if(isset($_POST['chamberName'])){
		$chamberName = $_POST['chamberName'];
	}else{
		$chamberName = "";
	}
	$firstname = array();
	$lastname = array();
	$studentMobile = array();
	$studentEmail = array();
	$studentDob = array();
	$studentGender = array();
	$altContact = array();
	$newFileName = array();
	$tempFiles = array();
	$totalUploadedFile = 2;
	$target_dir = "../../uploads/";
	$tempFiles[0] = explode(".", $_FILES["presentationUpload"]["name"]);
	$tempFiles[1] = explode(".", $_FILES["documentUpload"]["name"]);
	$tempFiles[2] = "";
	$newFileName[2] = "";
	if($_FILES["paymentUpload"]["name"]){
		$totalUploadedFile = 3;
		$tempFiles[2] = explode(".", $_FILES["paymentUpload"]["name"]);
	}

	for($i = 0; $i < $totalUploadedFile; $i++){
		$rand = rand(0, 999999999);
		$newFileName[$i] = $rand . $i . '.' . end($tempFiles[$i]);
		while(file_exists($newFileName[$i])){
			$rand = rand(0, 999999999);
		    $newFileName[$i] = $rand . $i . '.' . end($tempFiles[$i]);
		}
	}

	for($i = 1; $i < 6; $i++){
		if(isset($_POST['firstname' . $i])){
			$firstname[$i] = $_POST['firstname' . $i];
		}

		if(isset($_POST['lastname' . $i])){
			$lastname[$i] = $_POST['lastname' . $i];
		}

		if(isset($_POST['studentMobile' . $i])){
			$studentMobile[$i] = $_POST['studentMobile' . $i];
		}

		if(isset($_POST['studentEmail' . $i])){
			$studentEmail[$i] = $_POST['studentEmail' . $i];
		}

		if(isset($_POST['dob' . $i])){
			$studentDob[$i] = $_POST['dob' . $i];
		}

		if(isset($_POST['gender' . $i])){
			$studentGender[$i] = $_POST['gender' . $i];
		}

		if(isset($_POST['setContact' . $i])){
			$altContact[$i] = 1;
		}else{
			$altContact[$i] = 0;
		}
	}

	$accountId = $db->getRows('tbl_accounts', array('where'=>array('username'=>$_SESSION['username'])));
	$rowCount = $db->getRows('tbl_entries', array('where'=>array('category'=>$category, 'account_id'=>$accountId[0]['account_id']), 'return_type'=>'count'));

	
	if($rowCount >= 2){
		echo $rowCount;
		die();
	}

	$entryData = array(
		'account_id'=>$accountId[0]['account_id'],
		'category'=>$category,
		'title'=>$title,
		'description'=>$description,
		'college'=>$college,
		'address'=>$collegeAddress,
		'college_dean'=>$collegeDean,
		'coach'=>$coach,
		'contact_person'=>$contactPerson,
		'landline'=>$landline,
		'phone'=>$mobile,
		'email'=>$email,
		'chamberName'=>$chamberName,
		'attachPresentation'=>$newFileName[0],
		'attachDocu'=>$newFileName[1],
		'attachPayment'=>$newFileName[2],
		'status'=>'pending'
	);
	$insertEntry = $db->insert('tbl_entries', $entryData);

	$fetchEntryId = $db->getRows('tbl_entries', array('select'=>'entry_id', 'where'=>array('title'=>$title), 'limit'=>1));

	for($i = 1; $i < 6; $i++){
		if(isset($_POST['firstname' . $i]) && isset($_POST['lastname' . $i]) && isset($_POST['dob' . $i]) && isset($_POST['gender' . $i])){
			$studentsData = array(
				'entry_id'=>$fetchEntryId[0]['entry_id'],
				'firstname'=>$firstname[$i],
				'lastname'=>$lastname[$i],
				'email'=>$studentEmail[$i],
				'mobile'=>$studentMobile[$i],
				'birthdate'=>$studentDob[$i],
				'gender'=>$studentGender[$i],
				'contact_person'=>$altContact[$i]
			);
			$insertStudents = $db->insert('tbl_students', $studentsData);
		}
	}

	if(!$insertEntry){
		echo "fail";
	}else{
		move_uploaded_file($_FILES["presentationUpload"]["tmp_name"],$target_dir . $newFileName[0]);
		move_uploaded_file($_FILES["documentUpload"]["tmp_name"], $target_dir . $newFileName[1]);
		if($totalUploadedFile == 3){
			move_uploaded_file($_FILES["paymentUpload"]["tmp_name"], $target_dir . $newFileName[2]);
		}
		echo "success";
	}
?>