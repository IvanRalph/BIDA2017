<?php
	include "sql-statements.php";
	session_start();

	$db = new DB();

	$username = $_POST['username'];
	$schoolName = $_POST['schoolName'];
	$schoolAddress = $_POST['schoolAddress'];
	$schoolContact = $_POST['schoolNumber'];
	$schoolEmail = $_POST['schoolEmail'];
	$firstName = $_POST['firstName'];
	$lastName = $_POST['lastName'];

	$checkUser = $db->getRows('tbl_accounts', array('where'=>array('username'=>$username), 'return_type'=>'count'));

	if($checkUser > 0 && $username != $_SESSION['username']){
		echo "user exist";
		die();
	}


	$data = array(
		'username'=>$username,
		'school_name'=>$schoolName,
		'school_address'=>$schoolAddress,
		'contact_info'=>$schoolContact,
		'school_email'=>$schoolEmail,
		'first_name'=>$firstName,
		'last_name'=>$lastName
	);

	$updateProfile = $db->update('tbl_accounts', $data, array('username'=>$_SESSION['username']));

	if($updateProfile == false){
		echo $updateProfile;
	}else{
		$_SESSION['username'] = $username;
		echo "success";
	}
?>