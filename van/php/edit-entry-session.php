<?php
	include "sql-statements.php";
	$db = new DB();
	
	if (session_status() == 2) {
	    session_destroy();
	}
	session_start();

	$id = $_POST['id'];
	$_SESSION['editEntry'] = $db->getRows('tbl_entries', array('where'=>array('entry_id'=>$id)));;
	$numOfStudents = $db->getRows('tbl_students', array('where'=>array('entry_id'=>$id)));

	if($_SESSION['editEntry'] == "" || $_SESSION['editEntry'] == false){
		echo json_encode(array("status"=>"fail"));
	}else{
		echo json_encode($numOfStudents);
	}
?>