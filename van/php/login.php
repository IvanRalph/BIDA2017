<?php
	session_start();
	include "sql-statements.php";
	$db = new DB();

	$username = $_POST['username'];
	$password = md5(sha1($_POST['password']));

	$validateUser = $db->getRows('tbl_accounts', array('where'=>array('username'=>$username, 'password'=>$password)));
	if(count($validateUser) != 1){
		echo json_encode(array('status'=>'invalid', 'return'=>$validateUser));
	}elseif($validateUser[0]['username'] === $username){
		$_SESSION['username'] = $validateUser[0]['username'];
		$_SESSION['accountType'] = $validateUser[0]['account_type'];
		echo json_encode(array('status'=>'success', 'account_type'=>$validateUser[0]['account_type']));
	}else{
		echo json_encode(array('status'=>'invalid', 'return'=>$validateUser));
	}
?>