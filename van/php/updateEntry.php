<?php
	include "sql-statements.php";
	$db = new DB();
	session_start();

	$category = $_POST['category'];
	$title = $_POST['title'];
	$description = $_POST['description'];
	$college = $_POST['college'];
	$collegeDean = $_POST['collegeDean'];
	$collegeAddress = $_POST['collegeAddress'];
	$coach = $_POST['coach'];
	$contactPerson = $_POST['contactPerson'];
	$landline = $_POST['landline'];
	$mobile = $_POST['mobile'];
	$email = $_POST['email'];
	if(isset($_POST['chamberName'])){
		$chamberName = $_POST['chamberName'];
	}else{
		$chamberName = "";
	}
	$firstname = array();
	$lastname = array();
	$studentDob = array();
	$studentGender = array();
	$altContact = array();
	$newFileName = array();
	$tempFiles = array();
	$totalUploadedFile = 2;
	$target_dir = "../../uploads/";
	$tempFiles[0] = explode(".", $_FILES["presentationUpload"]["name"]);
	$tempFiles[1] = explode(".", $_FILES["documentUpload"]["name"]);
	$tempFiles[2] = "";
	$newFileName[2] = "";
	if($_FILES["paymentUpload"]["name"]){
		$totalUploadedFile = 3;
		$tempFiles[2] = explode(".", $_FILES["paymentUpload"]["name"]);
	}

	for($i = 0; $i < $totalUploadedFile; $i++){
		$rand = rand(0, 999999999);
		$newFileName[$i] = $rand . $i . '.' . end($tempFiles[$i]);
		while(file_exists($newFileName[$i])){
			$rand = rand(0, 999999999);
		    $newFileName[$i] = $rand . $i . '.' . end($tempFiles[$i]);
		}
	}

	for($i = 1; $i < 6; $i++){
		if(isset($_POST['firstname' . $i])){
			$firstname[$i] = $_POST['firstname' . $i];
		}

		if(isset($_POST['lastname' . $i])){
			$lastname[$i] = $_POST['lastname' . $i];
		}

		if(isset($_POST['dob' . $i])){
			$studentDob[$i] = $_POST['dob' . $i];
		}

		if(isset($_POST['gender' . $i])){
			$studentGender[$i] = $_POST['gender' . $i];
		}

		if(isset($_POST['setContact' . $i])){
			$altContact[$i] = 1;
		}else{
			$altContact[$i] = 0;
		}
	}
	$entryId = $_SESSION['editEntry'][0]['entry_id'];

	$updateData = array(
		'category'=>$category,
		'title'=>$title,
		'description'=>$description,
		'college'=>$college,
		'address'=>$collegeAddress,
		'college_dean'=>$collegeDean,
		'coach'=>$coach,
		'contact_person'=>$contactPerson,
		'landline'=>$landline,
		'phone'=>$mobile,
		'email'=>$email,
		'chamberName'=>$chamberName,
		'attachPresentation'=>$newFileName[0],
		'attachDocu'=>$newFileName[1],
		'attachPayment'=>$newFileName[2]
	);

	$condition = array(
		'entry_id'=>$entryId
	);

	$updateEntryResult = $db->update('tbl_entries', $updateData, $condition);

	if($updateEntryResult == false){
		echo $updateEntryResult;
	}else{
		echo "success";
	}
?>