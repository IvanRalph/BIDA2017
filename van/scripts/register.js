function reg_validation() {
	var con = confirm("Are you sure you want to submit?");

	if(con == true){
		reg_submit();
	}
}

function reg_submit(){
	var formData = $("#reg_form").serialize();
	console.log("reg submit entered");
	$.ajax({
		url: "van/php/register.php",
		type: 'POST',
		data: formData,
		success: function(data, result){
			console.log("Success entered");
			if(result != "success"){
				console.log("RESULT FAILED: " + data);
			}else{
				if(data == "user exists"){
					alert("User already exists");
				}else if(data == "success"){
					alert("Registration Successful");
					window.location.href = "index.html";
				}else{
					console.log("REGISTER FAILED: " + data);
				}
			}
		},
		error: function(xhr, status, thrown){
			console.log("ERROR: " + status + " THROW: " + thrown + "XHR: " + xhr.responseText);
		}
	});
}