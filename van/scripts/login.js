$(document).ready(function(){
	$("#login_form").submit(function(e){
		e.preventDefault();
		login_validation();
	});
});

function login_validation() {
	if(document.forms["login_form"]["username"].value == ""){
		alert("Please input a valid username.");
		return false;
	} else if(document.forms["login_form"]["password"].value == ""){
		alert("Please input a valid password.");
		return false;
	} else {
		form_submit();
	}
}

function form_submit(){
	var formData = $("#login_form").serialize();
	$("#loginBtn").html('Logging in...');
	$("#loginBtn").attr('disabled', 'disabled');
	$.ajax({
		url: "van/php/login.php",
		type: 'POST',
		dataType: 'json',
		data: formData,
		success: function(data, result){
			if(result != "success"){
				console.log("RESULT FAILED: " + result);
				$("#loginBtn").html('Sign In');
				$("#loginBtn").removeAttr('disabled');
			}else{
				if(data.status == "invalid"){
					alert("Username or Password does not match");
					$("#loginBtn").html('Sign In');
					$("#loginBtn").removeAttr('disabled');
					$("input[name='password']").val("");
				}else if(data.status == "success"){
					if(data.account_type == 'standard'){
						window.location.href = "rep/view_entry.php";
					}else if(data.account_type == 'admin'){
						window.location.href = "admin/admin_view.php";
					}
				}else{
					console.log("ERROR: " + data.status);
					$("#loginBtn").html('Sign In');
					$("#loginBtn").removeAttr('disabled');
				}
			}
		},
		error: function(xhr, status, thrown){
			console.log("ERROR: " + status + " THROW: " + thrown + "XHR: " + xhr.responseText);
			$("#loginBtn").html('Sign In');
			$("#loginBtn").removeAttr('disabled');
		}
	});
}