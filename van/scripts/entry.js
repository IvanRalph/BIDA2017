/*FORM VALIDATION*/
function RegistrationValidation(){
	if(document.forms["entry_form"]["entry_title"].value == ""){
		alert("Title must be filled out.");
		return false;
	} else if(document.forms["entry_form"]["entry_description"].value == ""){
		alert("Description must be filled out.");
		return false;
	} else if(document.forms["entry_form"]["entry_university"].value == ""){
		alert("College/University must be filled out.");
		return false;
	} else if(isNaN(document.forms["entry_form"]["entry_university"].value) == false){
		alert("Please enter a valid College/University.");
		return false;
	} else if(document.forms["entry_form"]["entry_address"].value == ""){
		alert("Address must be filled out.");
		return false;
	} else if(isNaN(document.forms["entry_form"]["entry_address"].value) == false){
		alert("Please enter a valid address.");
		return false;
	} else if(document.forms["entry_form"]["entry_dean"].value == ""){
		alert("Dean must be filled out.");
		return false;
	} else if(isNaN(document.forms["entry_form"]["entry_dean"].value) == false){
		alert("Please enter a valid dean name.");
		return false;
	} else if(document.forms["entry_form"]["entry_adviser"].value == ""){
		alert("Team Adviser/Coach must be filled out.");
		return false;
	} else if(isNaN(document.forms["entry_form"]["entry_adviser"].value) == false){
		alert("Please enter a valid adviser name.");
		return false;
	} else if(document.forms["entry_form"]["entry_contactperson"].value == ""){
		alert("Contact Person must be filled out.");
		return false;
	}  else if(isNaN(document.forms["entry_form"]["entry_contactperson"].value) == false){
		alert("Please enter a valid contact person.");
		return false;
	} else if(document.forms["entry_form"]["entry_landline"].value == ""){
		alert("Landline must be filled out.");
		return false;
	} else if(document.forms["entry_form"]["entry_landline"].value.length != 7){
		alert("Please enter 7 valid digits on landline.");
		return false;
	} else if(document.forms["entry_form"]["entry_mobile"].value == ""){
		alert("Mobile # must be filled out.");
		return false;
	} else if(document.forms["entry_form"]["entry_mobile"].value.length != 10){
		alert("Please enter 10 valid digits on mobile #.");
		return false;
	} else if(document.forms["entry_form"]["entry_email"].value == ""){
		alert("Email must be filled out.");
		return false;
	}
	
	if(document.forms["entry_form"]["entry_studentemail1"].value == "" && document.forms["entry_form"]["entry_studentemail2"].value == ""){
		alert("Atleast 1 student email must be filled out.");
		return false;
	} else if(document.forms["entry_form"]["entry_studentmobile1"].value == "" && document.forms["entry_form"]["entry_studentmobile2"].value == ""){
		alert("Atleast 1 student mobile # must be filled out.");
		return false;
	} else if(document.forms["entry_form"]["entry_studentmobile1"].value.length != 10 && document.forms["entry_form"]["entry_studentmobile1"].value != ""){
		alert("Please enter 10 valid digits on student mobile #1.");
		return false;
	} else if(document.forms["entry_form"]["entry_studentmobile2"].value.length != 10 && document.forms["entry_form"]["entry_studentmobile2"].value != ""){
		alert("Please enter 10 valid digits on student mobile #2.");
		return false;
	}
	
	var Confirmation = confirm("Are you sure about the details?");
	
	if (Confirmation == true) {
		$('#myModal').modal('show');
	} else {
		return false;
	}
}

$(document).ready(function(){
	var currentStud = 1;
	if(currentStud == 1){
		$("#subStudent").attr('disabled', 'disabled');
	}
	$("#addStudent").click(function(e){
		var currentStud = $('#stud_form div').length;
		if(currentStud < 5){
			currentStud++;
			$("#stud_form").append('<div id="stud'+ currentStud +'">' +
                                '<h3>Student '+ currentStud +'</h3>' +
                                'First Name:<input type="text" class="form-control input-sm" name="firstname'+ currentStud +'"><br>'+
                                'Last Name:<input type="text" class="form-control input-sm" name="lastname'+ currentStud +'"><br>' +
                                'E-Mail: <input type="email" class="form-control input-sm" name="studentEmail'+ currentStud +'"><br>' +
                                'Mobile: <input type="number" class="form-control input-sm" name="studentMobile'+ currentStud +'"><br>' +
                                'Date of Birth: <input type="date" class="form-control input-sm" name="dob'+ currentStud +'"><br>' +
                                'Gender:' +
                                '<label class="radio-inline">' +
                                    '<input type="radio" name="gender'+ currentStud +'" value="Male"> Male' +
                                '</label>' +
                                '<label class="radio-inline">' +
                                    '<input type="radio" name="gender'+ currentStud +'" value="Female"> Female' +
                                '</label><br>' +
                                '<label class="checkbox-inline">' +
                                    '<input type="checkbox" name="setContact'+ currentStud +'" class="setContact"> Set as Contact person? ' +
                                '</label><br>' +
                                '<input type="button" class="btn btn-warning btn-xs" id="removeStud'+ currentStud +'" value="Clear">' +
                            '</div>');
			if(currentStud >= 5){
				$("#addStudent").attr('disabled', 'disabled');
			}
			$("#subStudent").removeAttr('disabled');
		}else if(currentStud == 1){
			$("#subStudent").attr('disabled', 'disabled');
		}
		console.log(currentStud);
		$("#removeStud" + currentStud).click(function(){
			$("#stud"+ currentStud +" input").val('');
			$("#stud"+ currentStud +" input[type='radio'").prop('checked', false);
			$(this).val('Clear');
		});
	});

	$("#removeStud1").click(function(){
			$("#stud1 input").val('');
			$("#stud1 input[type='radio'").prop('checked', false);
			$(this).val('Clear');
		});

	$("#subStudent").click(function(){
		if($('#stud_form div').length - 1 == 1){
			$("#subStudent").attr('disabled', 'disabled');
		}
		$('#stud_form').children().last().remove();
		$("#addStudent").removeAttr('disabled');
	});

	$("#submitEntry").click(function(){
		var con = confirm("Submit entry?");

		if(con == true){
			submit_entry();
		}
	});
});

function editEntry(entryid){
	$.ajax({
		url: "populateData.php",
		type: "post",
		data: "entryid=" + entryid,
		success: function(data, result){
			if(result != "success"){
				console.log("RESULT FAILED: " + data);
			}else{
				if(data == "success"){
					
				}else{
					console.log("PHP ERROR: " + data);
				}
			}
		},
		error: function(result){
			console.log(result);
		}
	});
}

function submit_entry(){
	var formData = $("#entry_form").serialize();
	$.ajax({
		url: "php/entry.php",
		type: "POST",
		data: formData,
		success: function(data, result){
			if(result != "success"){
				console.log("RESULT FAILED: " + data);
			}else{
				if(data == "success"){
					alert("Entry submitted successfully!");
					window.location.href = "../index.html";
				}else{
					console.log("PHP ERROR: " + data);
				}
			}
		},
		error: function(result){
			console.log(result);
		}
	});
}