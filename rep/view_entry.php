<?php
    session_start();
    if(!isset($_SESSION['username']) || $_SESSION['accountType'] !== 'standard'){
        header("location: ../index.php");
        die();
    }
    include "../van/php/sql-statements.php";
    

    $db = new DB();

    $accountId = $db->getRows('tbl_accounts', array('where'=>array('username'=>$_SESSION['username'])));
    $countEntry = $db->getRows('tbl_entries', array('where'=>array('account_id'=>$accountId[0]['account_id']), 'return_type'=>'count'));

    function countEntry($db, $category, $accountId){
      $countEntry = $db->getRows('tbl_entries', array('where'=>array('account_id'=>$accountId, 'category'=>$category), 'return_type'=>'count'));
      return $countEntry;
    }

    function getEntry($db, $category, $accountId){
      $entryData = $db->getRows('tbl_entries', array('where'=>array('category'=>$category, 'account_id'=>$accountId[0]['account_id'])));
      return $entryData;
    }
    $foodCount = countEntry($db, 'Food', $accountId[0]['account_id']);
    $nonFoodCount = countEntry($db, 'Non-Food', $accountId[0]['account_id']);
    $servicesCount = countEntry($db, 'Services', $accountId[0]['account_id']);
    $techCount = countEntry($db, 'Technology', $accountId[0]['account_id']);

    $foodResult = getEntry($db, 'Food', $accountId);
    $nonFoodResult = getEntry($db, 'Non-Food', $accountId);
    $servicesResult = getEntry($db, 'Services', $accountId);
    $techResult = getEntry($db, 'Technology', $accountId);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>11th BIDA Award 2017 | View Entries</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- fullCalendar 2.2.5-->
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- jQuery 2.2.3 -->
  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script type="text/javascript" src="scripts/view-entry.js"></script>
  <script type="text/javascript" src="scripts/entry.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Entry</h4>
        </div>
        <div id="editEntryModal" class="modal-body" hidden>
          <form id="updateEntryForm">
            <!-- CATEGORY --> 
            <div id="page1" class="row" style="text-align: center;">
                <h2 style="text-align: center;">Category</h2>
                <div class="btn-group" data-toggle="buttons" >
                  <label class="btn btn-primary <?php echo $_SESSION['editEntry'][0]['category']=='Food'?'active':''; ?>">
                    <input type="radio" name="category" id="food" autocomplete="off" <?php echo $_SESSION['editEntry'][0]['category']=='Food'?'checked':''; ?> value="Food"> Food
                  </label>
                  <label class="btn btn-primary <?php echo $_SESSION['editEntry'][0]['category']=='Non-Food'?'active':''; ?>">
                    <input type="radio" name="category" id="non-food" autocomplete="off" <?php echo $_SESSION['editEntry'][0]['category']=='Non-Food'?'checked':''; ?> value="Non-Food"> Non-Food
                  </label>
                  <label class="btn btn-primary <?php echo $_SESSION['editEntry'][0]['category']=='Services'?'active':''; ?>">
                    <input type="radio" name="category" id="services" autocomplete="off" <?php echo $_SESSION['editEntry'][0]['category']=='Services'?'checked':''; ?> value="Services"> Services
                  </label>
                  <label class="btn btn-primary <?php echo $_SESSION['editEntry'][0]['category']=='Technology'?'active':''; ?>">
                    <input type="radio" name="category" id="technology" autocomplete="off" <?php echo $_SESSION['editEntry'][0]['category']=='Technology'?'checked':''; ?> value="Technology"> Technology
                  </label>
                </div>

                <div class="box-footer">
                  <div class="pull-right">
                    <button type="button" id="next1" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Next</button>
                  </div>
                </div>
                <!-- /.box-footer -->
            </div>

            <!-- TITLE -->
            <div id="page2" class="row col-xs-6" style="text-align: center; margin-left: 25%; float: none;" hidden>
                <h2 >Title</h2>
                Title: <input type="text" class="form-control" name="title" value="<?php echo $_SESSION['editEntry'][0]['title']; ?>"><br>
                Description: <textarea rows="5" class="form-control" name="description"><?php echo $_SESSION['editEntry'][0]['description']; ?></textarea><br>

                <div class="box-footer">
                  <div class="pull-left">
                    <button class="btn btn-default" id="back2"><i class="glyphicon glyphicon-chevron-left"></i> Previous</button>
                  </div>
                  <div class="pull-right">
                    <button type="button" id="next2" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Next</button>
                  </div>
                </div>
            </div>

            <!-- TEAM INFORMATION -->
            <div class="row  col-xs-6" id="page3" style="text-align: center; margin-left: 25%; float: none;" hidden>
                <h2>Team Information</h2>
                College/University: <input type="text" class="form-control" name="college" value="<?php echo $_SESSION['editEntry'][0]['college']; ?>"><br>
                College/University's Address: <input type="text"  class="form-control" name="collegeAddress" value="<?php echo $_SESSION['editEntry'][0]['address']; ?>"><br>
                College Dean: <input type="text" class="form-control" name="collegeDean" value="<?php echo $_SESSION['editEntry'][0]['college_dean']; ?>"><br>
                Team Adviser/Coach: <input type="text" class="form-control" name="coach" value="<?php echo $_SESSION['editEntry'][0]['coach']; ?>"><br>
                Contact Person: <input type="text" class="form-control" name="contactPerson" value="<?php echo $_SESSION['editEntry'][0]['contact_person']; ?>"><br>
                Landline: <input type="number" class="form-control" name="landline" value="<?php echo $_SESSION['editEntry'][0]['landline']; ?>"><br>
                Mobile #: <input type="number" class="form-control" name="mobile" value="<?php echo $_SESSION['editEntry'][0]['phone']; ?>"><br>
                E-Mail: <input type="email" class="form-control" name="email" value="<?php echo $_SESSION['editEntry'][0]['email']; ?>"><br>
                <div class="box-footer">
                  <div class="pull-left">
                    <button class="btn btn-default" id="back3"><i class="glyphicon glyphicon-chevron-left"></i> Previous</button>
                  </div>
                  <div class="pull-right">
                    <button type="button" id="next3" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Next</button>
                  </div>
                </div>
            </div>

            <!-- STUDENTS -->
            <div class="row col-xs-6" id="page4" style="text-align: center; margin-left: 25%; float: none;" hidden>
            <p>NOTE: Please select at least 2 alternative contact persons.</p>
                <div id="students">
                    <input type="button" id="addStudent" class="btn btn-success" name="addStudent" value="+">
                    <input type="button" id="subStudent" class="btn btn-danger" name="subStudent" value="-">
                    <!-- <div id="stud1" class="well">
                        <h3>Student 1</h3>
                        First Name:<input type="text" class="form-control input-sm" name="firstname1"><br>
                        Last Name:<input type="text" class="form-control input-sm" name="lastname1"><br>
                        E-Mail: <input type="email" class="form-control input-sm" name="studentEmail1"><br>
                        Mobile: <input type="number" class="form-control input-sm" name="studentMobile1"><br>
                        Date of Birth: <input type="date" class="form-control input-sm" name="dob1"><br>
                        Gender:
                        <label class="radio-inline">
                            <input type="radio" name="gender1" value="Male"> Male
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="gender1" value="Female"> Female
                        </label><br>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="setContact1" class="setContact"> Set as Contact person? 
                        </label><br>
                        <input type="button" class="btn btn-warning btn-xs" id="removeStud1" value="Clear">
                    </div> -->
                </div>
                <div class="box-footer">
                  <div class="pull-left">
                    <button class="btn btn-default" id="back4"><i class="glyphicon glyphicon-chevron-left"></i> Previous</button>
                  </div>
                  <div class="pull-right">
                    <button type="button" id="next4" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Next</button>
                  </div>
                </div>
            </div>

            <!-- ATTACHMENTS -->
            <div class="row" id="page5" style="text-align: center; float: none;" hidden>
              <div class="form-group">
                <div class="btn btn-default btn-file">
                  <i class="fa fa-paperclip"></i>* Attach Presentation
                  <input type="file" name="presentationUpload">
                </div>
                <div class="btn btn-default btn-file">
                  <i class="fa fa-paperclip"></i>* Attach Document
                  <input type="file" name="documentUpload">
                </div>
                <div class="btn btn-default btn-file">
                  <i class="fa fa-paperclip"></i> Attach Payment proof
                  <input type="file" name="paymentUpload">
                </div>
                <p class="help-block">Max. 32MB<br>* - Required uploads</p><br>NOTE: Due to security concerns, it is mandatory to re-upload your files.
              </div>
                <div class="box-footer">
                  <div class="pull-left">
                    <button class="btn btn-default" id="back5"><i class="glyphicon glyphicon-chevron-left"></i> Previous</button>
                  </div>
                  <div class="pull-right">
                    <button type="button" id="next5" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Next</button>
                  </div>
                </div>
            </div>

            <!-- CERTIFICATIONS -->
            <div class="row" id="page6" style="text-align: center; float: none;" hidden>
                <h3>Certifications</h3>
                <h4>A. SCHOOL and/or LOCAL CHAMBER</h4>
                <h5><u>School</u></h5>
                <p>I hereby certify that the entry entitled <i class="certTitle"></i> is the original work of our student/s who is/are currently   enrolled in this school. <br><br>
                <u><i id="certDean" style="text-align: right;"></i></u><br><b>COLLEGE DEAN </b>
                </p><br>
                <h5><u>Or Local Chamber</u></h5>
                <p>The <input type="text" name="chamberName" placeholder="Chamber Name"> is endorsing and/or sponsoring the presentation of the entry of <i class="certTitle"></i>. </p><br>
                <h4>B. SWORN STATEMENT OF THE TEAM</h4>
                <ul style="text-align: left;">
                    <li>The business idea was originally created by the undersigned student/s. </li>
                    <li>The business idea has not been previously entered in any national competition. </li>
                    <li>I am/We are prepared to make up into  my/our  business idea if selected by the Board of Judges and to deliver such business plan to PCCI for final judging.  I/We  understand that if  my/our  business idea is chosen for an Award,  my/our  business plan can be retrieved by me/the group  and the idea made up from it remains  my/our  property.</li>
                </ul>

                <div style="text-align: left;">
                    <h3>Students</h3>
                    <ul id="studentsList">
                        
                    </ul>

                    <h3>Coach</h3>
                    <p id="certCoach"></p>
                </div>

                <input type="checkbox" name="agreeTerms"> I agree to the terms and conditions.<br>

                <div class="box-footer">
                  <div class="pull-left">
                    <button class="btn btn-default" id="back6"><i class="glyphicon glyphicon-chevron-left"></i> Previous</button>
                  </div>
                  <div class="pull-right">
                    <button type="button" id="entryUpdate" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Update entry</button>
                  </div>
                </div>

            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <header class="main-header">

    <!-- Logo -->
    <a href = "../index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="../img/bida_logo.png" style="height: 40px; width: 40px;"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="../img/bida_logo.png" style="height: 40px; width: 40px;"><b>User</b>Dashboard</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li>
                <a href="change_pass.php">
                    Account Settings <i class="fa fa-gear"></i>
                </a>
            </li>
          <li class="dropdown tasks-menu">
            <!-- Menu Toggle Button -->
            <a href="../index.php">
              Log Out <i class="fa fa-sign-out"></i>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header" style="font-size: 23px; color: #ddd;">
          Welcome, <?php echo $_SESSION['username']; ?>!
        </li>
        <li class="header">INFORMATIONS</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="view_entry.php"><i class="fa fa-search-plus"></i> <span>View Entries</span></a></li>
        <li><a href="submit_entries.php"><i class="fa fa-check"></i> <span>Submit Entries</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Entries
        <small>You have submitted <?php if(!$countEntry || is_array($countEntry)){ echo "0";}else{echo $countEntry;} ?> <?php if($countEntry < 2){echo "entry.";}else{ echo "entries.";} ?></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <a href="submit_entries.php" class="btn btn-primary btn-block margin-bottom">Submit an Entry</a>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Categories</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul id="categoryFilter" class="nav nav-pills nav-stacked">
                <li class="active" id="foodActive"><a href="" id="foodClick"><i class="glyphicon glyphicon-apple"></i> Food <span class="badge ivanBadge"><?php echo $foodCount; ?></span>
                  	</a></li>
                <li id="nonFoodActive"><a href="" id="nonFoodClick"><i class="glyphicon glyphicon-th-large"></i>Non-Food <span class="badge ivanBadge"><?php echo $nonFoodCount; ?></span>
                	</a></li>
                <li id="servicesActive"><a href="" id="servicesClick"><i class="glyphicon glyphicon-briefcase"></i>Services <span class="badge ivanBadge"><?php echo $servicesCount; ?></span>
                	</a></li>
                <li id="techActive"><a href="" id="techClick"><i class="glyphicon glyphicon-console"></i>Technology <span class="badge ivanBadge"><?php echo $techCount; ?></span>
                	</a>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
          <!-- <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Teams</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#"><i class="fa fa-circle-o text-red"></i> Team1
                <span class="label label-warning pull-right">65</span></a></li>
                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Team2
                <span class="label label-warning pull-right">65</span></a></li>
                <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Team3
                <span class="label label-warning pull-right">65</span></a></li>
              </ul>
            </div>
             /.box-body
          </div> -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Inbox</h3>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->

                <!-- /.pull-right -->
              </div>
              <div id="tableViews" class="table-responsive mailbox-messages">
                <table id="foodTable" class="table table-hover table-striped">
                  <thead>
                    <td>College</td>
                    <td>Title</td>
                    <td>Payment Attachment</td>
                  </thead>
                  <tbody>
                  <?php
                    if(!empty($foodResult)){ $count = 0; foreach ($foodResult as $food) {
                        $count++;
                  ?>
                    <tr>
                      <td class="mailbox-name"><a href="" class="editEntry" id="<?php echo $food['entry_id']; ?>"><?php echo $food['college'] ?></a></td>
                      <td class="mailbox-subject"><b><?php echo $food['title'] ?></b>
                      </td>
                      <td class="mailbox-attachment">
                      <?php
                        if($food['attachPayment'] != ""){
                          ?>
                          <i class="fa fa-paperclip"></i>
                          <?php
                        }
                      ?>
                      </td>
                    </tr>
                  <?php
                  } }else{ ?>
                    <td>No entries to display...</td>
                    <td></td>
                    <td></td>
                  <?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
                <table id="nonFoodTable" class="table table-hover table-striped" hidden>
                  <thead>
                    <td>College</td>
                    <td>Title</td>
                    <td>Payment Attachment</td>
                  </thead>
                  <tbody>
                  <?php
                    if(!empty($nonFoodResult)){ $count = 0; foreach ($nonFoodResult as $nonFood) {
                        $count++;
                  ?>
                    <tr>
                      <td class="mailbox-name"><a href="" class="editEntry" id="<?php echo $nonFood['entry_id']; ?>"><?php echo $nonFood['college'] ?></a></td>
                      <td class="mailbox-subject"><b><?php echo $nonFood['title'] ?></b>
                      </td>
                      <td class="mailbox-attachment">
                      <?php
                        if($nonFood['attachPayment'] != ""){
                          ?>
                          <i class="fa fa-paperclip"></i>
                          <?php
                        }
                      ?>
                      </td>
                    </tr>
                  <?php
                  } }else{ ?>
                    <td>No entries to display...</td>
                    <td></td>
                    <td></td>
                  <?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
                <table id="servicesTable" class="table table-hover table-striped" hidden>
                  <thead>
                    <td>College</td>
                    <td>Title</td>
                    <td>Payment Attachment</td>
                  </thead>
                  <tbody>
                  <?php
                    if(!empty($servicesResult)){ $count = 0; foreach ($servicesResult as $services) {
                        $count++;
                  ?>
                    <tr>
                      <td class="mailbox-name"><a href="" class="editEntry" id="<?php echo $services['entry_id']; ?>"><?php echo $services['college'] ?></a></td>
                      <td class="mailbox-subject"><b><?php echo $services['title'] ?></b>
                      </td>
                      <td class="mailbox-attachment">
                      <?php
                        if($services['attachPayment'] != ""){
                          ?>
                          <i class="fa fa-paperclip"></i>
                          <?php
                        }
                      ?>
                      </td>
                    </tr>
                  <?php
                  } }else{ ?>
                    <td>No entries to display...</td>
                    <td></td>
                    <td></td>
                  <?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
                <table id="techTable" class="table table-hover table-striped" hidden>
                  <thead>
                    <td>College</td>
                    <td>Title</td>
                    <td>Payment Attachment</td>
                  </thead>
                  <tbody>
                  <?php
                    if(!empty($techResult)){ $count = 0; foreach ($techResult as $tech) {
                        $count++;
                  ?>
                    <tr>
                      <td class="mailbox-name"><a href="" class="editEntry" id="<?php echo $tech['entry_id']; ?>"><?php echo $tech['college'] ?></a></td>
                      <td class="mailbox-subject"><b><?php echo $tech['title'] ?></b>
                      </td>
                      <td class="mailbox-attachment">
                      <?php
                        if($tech['attachPayment'] != ""){
                          ?>
                          <i class="fa fa-paperclip"></i>
                          <?php
                        }
                      ?>
                      </td>
                    </tr>
                  <?php
                  } }else{ ?>
                    <td>No entries to display...</td>
                    <td></td>
                    <td></td>
                  <?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <div class="mailbox-controls">

                <!-- /.pull-right -->
              </div>
            </div>
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      <img src="../img/PCCI_logo.png" style="height: 20px;">
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="#">FEU - Makati</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- Page Script -->
<script>
  $(function () {
    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });

    //Enable check and uncheck all functionality
    $(".checkbox-toggle").click(function () {
      var clicks = $(this).data('clicks');
      if (clicks) {
        //Uncheck all checkboxes
        $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
        $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
      } else {
        //Check all checkboxes
        $(".mailbox-messages input[type='checkbox']").iCheck("check");
        $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
      }
      $(this).data("clicks", !clicks);
    });

    //Handle starring for glyphicon and font awesome
    $(".mailbox-star").click(function (e) {
      e.preventDefault();
      //detect type
      var $this = $(this).find("a > i");
      var glyph = $this.hasClass("glyphicon");
      var fa = $this.hasClass("fa");

      //Switch states
      if (glyph) {
        $this.toggleClass("glyphicon-star");
        $this.toggleClass("glyphicon-star-empty");
      }

      if (fa) {
        $this.toggleClass("fa-star");
        $this.toggleClass("fa-star-o");
      }
    });
  });
</script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
