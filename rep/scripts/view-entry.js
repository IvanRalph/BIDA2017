$(document).ready(function(){
	$("#foodClick").click(function(e){
		e.preventDefault();
		$("#tableViews table").attr("hidden", "hidden");
		$("#foodTable").removeAttr("hidden");
		$("#categoryFilter li").removeAttr("class", "class");
		$("#foodActive").attr("class", "active");
	});

	$("#nonFoodClick").click(function(e){
		e.preventDefault();
		$("#tableViews table").attr("hidden", "hidden");
		$("#nonFoodTable:hidden").removeAttr("hidden");
		$("#categoryFilter li").removeAttr("class", "class");
		$("#nonFoodActive").attr("class", "active");
	});

	$("#servicesClick").click(function(e){
		e.preventDefault();
		$("#tableViews table").attr("hidden", "hidden");
		$("#servicesTable").removeAttr("hidden");
		$("#categoryFilter li").removeAttr("class", "class");
		$("#servicesActive").attr("class", "active");
	});

	$("#techClick").click(function(e){
		e.preventDefault();
		$("#tableViews table").attr("hidden", "hidden");
		$("#techTable").removeAttr("hidden");
		$("#categoryFilter li").removeAttr("class", "class");
		$("#techActive").attr("class", "active");
	});
});