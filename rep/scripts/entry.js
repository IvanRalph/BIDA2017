var contactPersonCount = 0;
var currentStud = $('#students div').length;
if(currentStud == 1){
	$("#subStudent").attr('disabled', 'disabled');
}

$(document).ready(function(){
	$("#addStudent").click(function (e) {
		e.preventDefault();
        var currentStud = $('#students div').length;
        if(currentStud < 5){
            currentStud++;
            $("#students").append('<div id="stud'+ currentStud +'" class="well">' +
                '<h3>Student '+ currentStud +'</h3>' +
                'First Name:<input type="text" class="form-control input-sm" name="firstname'+ currentStud +'"><br>'+
                'Last Name:<input type="text" class="form-control input-sm" name="lastname'+ currentStud +'"><br>' +
                'E-Mail: <input type="email" class="form-control input-sm" name="studentEmail'+ currentStud +'"><br>' +
                'Mobile: <input type="number" class="form-control input-sm" name="studentMobile'+ currentStud +'"><br>' +
                'Date of Birth: <input type="date" class="form-control input-sm" name="dob'+ currentStud +'"><br>' +
                'Gender:' +
                '<label class="radio-inline">' +
                '<input type="radio" name="gender'+ currentStud +'" value="Male"> Male' +
                '</label>' +
                '<label class="radio-inline">' +
                '<input type="radio" name="gender'+ currentStud +'" value="Female"> Female' +
                '</label><br>' +
                '<label class="checkbox-inline">' +
                '<input type="checkbox" name="setContact'+ currentStud +'" class="setContact"> Set as Contact person? ' +
                '</label><br>' +
                '<input type="button" class="btn btn-warning btn-xs" id="removeStud'+ currentStud +'" value="Clear">' +
                '</div>');
            if(currentStud >= 5){
                $("#addStudent").attr('disabled', 'disabled');
            }
            $("#subStudent").removeAttr('disabled');
        }else if(currentStud == 1){
            $("#subStudent").attr('disabled', 'disabled');
        }
        console.log(currentStud);
        $("#removeStud" + currentStud).click(function(){
            $("#stud"+ currentStud +" input").val('');
            $("#stud"+ currentStud +" input[type='radio'").prop('checked', false);
            $(this).val('Clear');
        });
    });

	$(document).on('change', "#students input[class='setContact']", function (e) {
        console.log("state changed");
        console.log($('#students input[class="setContact"]:checked').length);
        if($('#students input[class="setContact"]:checked').length > 2){
            $(this).prop('checked', false);
            alert("Maximum of 2 alternative contact persons only..");
        }
        contactPersonCount = $('#students input[class="setContact"]:checked').length;
    });

	$("#removeStud1").click(function (e) {
		e.preventDefault();
        $("#stud1 input").val('');
        $("#stud1 input[type='radio']").prop('checked', false);
        $(this).val('Clear');
    });

	$("#subStudent").click(function (e) {
		e.preventDefault();
        if($('#students div').length - 1 === 1){
            $("#subStudent").attr('disabled', 'disabled');
        }
        $('#students').children().last().remove();
        $("#addStudent").removeAttr('disabled');
    });

	$("#submitEntry").click(function (e) {
		e.preventDefault();
        var con = confirm("Submit entry?");

        if(con == true){
            entrySubmission();
        }
    });

	$("#entrySubmit").click(function (e) {
		e.preventDefault();
        if($('input[name="agreeTerms"]').is(':checked')) {
            var con = confirm("Are you sure you want to submit?");
            if(con == true){
                entrySubmission();
            }
        }else{
            alert("Please agree to the terms and conditions to continue..");
        }
    });

	$("#entryUpdate").click(function (e) {
		e.preventDefault();
        if($('input[name="agreeTerms"]').is(':checked')) {
            var con = confirm("Are you sure you want to update your entry?");
            if(con == true){
                entryUpdate();
            }
        }else{
            alert("Please agree to the terms and conditions to continue..");
        }
    });

	$('#next1, #next2, #next3, #next4, #next5').click(function (e) {
        e.preventDefault();
        console.log('next clicked');
        nextPage($(this).attr('id'));
    });

	$('#back2, #back3, #back4, #back5, #back6').click(function (e) {
        e.preventDefault();
        var num = 0;
        switch($(this).attr("id")){
            case 'back2':
                num = 2;
                break;
            case 'back3':
                num = 3;
                break;
            case 'back4':
                num = 4;
                break;
            case 'back5':
                num = 5;
                break;
            case 'back6':
                num = 6;
                break;
        }
        $("#page" + num).attr("hidden", "hidden");
        $("#page" + (num - 1)).removeAttr("hidden");
    })
});

$(document).on('click', '.editEntry', function(e){
	e.preventDefault();
	var id = $(this).attr('id');
	editEntry(id);
	$("body").css("cursor", "progress");
	$(this).css('pointer-events', 'none');
});

$('#myModal').on('hidden.bs.modal', function (e) {
  location.reload();
});

function nextPage(selector){
    var validateNext = 2;
	switch(selector){
		case 'next1':
            $("#page1").attr("hidden", "hidden");
            $("#page2").removeAttr("hidden");
            break;
		case 'next2':
            if($("input[name='title']").val() == '' || $("textarea[name='description']").val() == ''){
                alert("Fill up the fields to continue..");
            }else{
                $("#page2").attr("hidden", "hidden");
                $("#page3").removeAttr("hidden");
                $(".certTitle").html($('#page2 input[name="title"]').val());
            }
            break;
		case 'next3':
            if($("input[name='college']").val() == '' || $("input[name='collegeAddress']").val() == '' || $("input[name='collegeDean']").val() == '' || $("input[name='coach']").val() == '' || $("input[name='contactPerson']").val() == '' || $("input[name='landline']").val() == '' || $("input[name='mobile']").val() == '' || $("input[name='email']").val() == ''){
                alert("Fill up the fields to continue..");
            }else{
                $("#page3").attr("hidden", "hidden");
                $("#page4").removeAttr("hidden");
                $("#certDean").html($("input[name='collegeDean']").val().toUpperCase());
                $("#certCoach").html($("input[name='coach']").val());
            }
            break;
		case 'next4':
            var empty = $("#students").find("input").filter(function() {
                if($(".setContact").val() == ""){
                    return this.value = 1;
                }else{
                    return this.value === "";
                }
            });
            if(contactPersonCount < 2){
                alert("Please select at least 2 alternative contact persons..");
            }else if($("input[name='firstname']").val() == '' || $("input[name='lastname']").val() == '' || $("input[name='email']").val() == '' || $("input[name='studentMobile']").val() == '' || $("input[name='dob']").val() == '' || empty.length){
                alert("Fill up the fields to continue..");
            }else{
                $("#page4").attr("hidden", "hidden");
                $("#page5").removeAttr("hidden");
                var currentCount = $("#students div").length;
                var studentsList = [];
                console.log(currentCount);
                for(var i = 1; i <= currentCount; i++){
                    studentsList[i] = $("#students input[name='firstname"+i+"']").val() + " " + $("#students input[name='lastname"+i+"']").val();

                }
                $("#studentsList").children().remove();
                for(var i = 1; i <= currentCount; i++){
                    $("#studentsList").append("<li>"+ studentsList[i] +"</li>");
                }
            }
            break;
		case 'next5':
            if($("input[name='presentationUpload']").val() == '' || $("input[name='documentUpload']").val() == ''){
                alert("Fill up the fields to continue..");
            }else{
                $("#page5").attr("hidden", "hidden");
                $("#page6").removeAttr("hidden");
            }
            break;
		default:
			console.log("Invalid Selector: " + selector);
			break;
	}
}

function editEntry(id){
	console.log("edit entry entered");
	$.ajax({
		url: '../van/php/edit-entry-session.php',
		type: 'POST',
		data: 'id=' + id,
		dataType: 'JSON',
		success: function(data, status){
			if(status == "success"){
				if(data.status != "fail"){
					var myData = new Array();
					myData = data;
					console.log(myData);
					$("#myModal").load(location.href+" #myModal>*","", function(){
						$('#myModal').modal('show');
						fill_students(data.length, myData);
						load_js("scripts/entry.js");
						setTimeout(function(){
							$("#editEntryModal").removeAttr("hidden");
						}, 500);
						$("body").css("cursor", "default");
						$("#" + id).css('pointer-events', 'auto');
					});
				}else{
					alert("Internal Server error, check logs for info..");
					console.log(data);
				}
			}else{
				console.log(status);
			}
		},
		error: function(xhr, status, thrown){
			console.log("ERROR: " + status + " THROW: " + thrown + "XHR: " + xhr.responseText);
		}
	});
}

function entryUpdate(){
	var formData = new FormData($("#updateEntryForm")[0]);
	console.log(formData);
	$.ajax({
		url: '../van/php/updateEntry.php',
		type: 'POST',
		data: formData,
		success: function(data, status){
			if(status == "success"){
				if(data == "success"){
					alert("Your entry has been updated..");
					window.location.href = "view_entry.php";
				}else{
					alert("Internal Server error, check logs for info..");
					console.log(data);
				}
			}else{
				console.log(status);
			}
		},
		error: function(xhr, status, thrown){
			console.log("ERROR: " + status + " THROW: " + thrown + "XHR: " + xhr.responseText);
		},
		cache: false,
        contentType: false,
        processData: false
	});
}

function entrySubmission(){
	// var formData = $("#entryForm").serialize();
	var formData = new FormData($("#entryForm")[0]);
	console.log(formData);
	$.ajax({
		url: '../van/php/entry.php',
		type: 'POST',
		data: formData,
		success: function(data, status){
			if(status == "success"){
				if(data == "success"){
					alert("You entry has been submitted, it will be reviewed by an administrator..");
					window.location.href = "view_entry.php";
				}else if(data == "over"){
					alert("You have reached the maximum of 2 entries on this category, either choose another category or delete an existing entry.");
				}else{
					console.log(data);
				}
			}else{
				console.log(status);
			}
		},
		error: function(status){

		},
		cache: false,
        contentType: false,
        processData: false
	});
}

function load_js(myScript)
   {
      var head= document.getElementsByTagName('head')[0];
      var script= document.createElement('script');
      script.type= 'text/javascript';
      script.src= myScript;
      head.appendChild(script);
   }

function fill_students(count, data){
	console.log(count);
	for(var i = 1; i < count + 1; i++){		
		$("#students").append('<div id="stud'+ i +'" class="well">' +
		            '<h3>Student '+ i +'</h3>' +
		            'First Name:<input type="text" class="form-control input-sm" name="firstname'+ i +'" value="'+ data[i-1].firstname +'"><br>'+
		            'Last Name:<input type="text" class="form-control input-sm" name="lastname'+ i +'" value="'+ data[i-1].lastname +'"><br>' +
		            'E-Mail: <input type="email" class="form-control input-sm" name="studentEmail'+ i +'" value="'+ data[i-1].email +'"><br>' +
		            'Mobile: <input type="number" class="form-control input-sm" name="studentMobile'+ i +'" value="'+ data[i-1].mobile +'"><br>' +
		            'Date of Birth: <input type="date" class="form-control input-sm" name="dob'+ i +'" value="'+ data[i-1].birthdate +'"><br>' +
		            'Gender:' +
		            '<label class="radio-inline">' +
		                '<input type="radio" name="gender'+ i +'" value="Male"> Male' +
		            '</label>' +
		            '<label class="radio-inline">' +
		                '<input type="radio" name="gender'+ i +'" value="Female"> Female' +
		            '</label><br>' +
		            '<label class="checkbox-inline">' +
		                '<input type="checkbox" name="setContact'+ i +'" class="setContact"> Set as Contact person? ' +
		            '</label><br>' +
		            '<input type="button" class="btn btn-warning btn-xs" id="removeStud'+ i +'" value="Clear">' +
		        '</div>');
	}
}

$("#changePassForm2").on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        url: "../van/php/changePassword.php",
        type: "POST",
        data: $(this).serialize(),
        success: function(data, result){
            if(result !== "success"){
                console.log("RESULT FAILED: " + data);
            }else{
                if(data === 'success'){
                    alert("Password changed successfully!");
                    window.location.href = "view_entry.php";
                }else{
                    alert(data);
                }
            }
        },
        error: function(xhr, status, thrown){
            console.log("ERROR: " + status + " THROW: " + thrown + " XHR: " + xhr.responseText);
        }
    });
});

$("#accountSettings").on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        url: "../van/php/updateProfile.php",
        type: "POST",
        data: $(this).serialize(),
        success: function(data, result){
            if(result !== "success"){
                console.log("RESULT FAILED: " + data);
            }else{
                if(data === 'success'){
                    alert("Account Updated successfully!");
                    window.location.href = "view_entry.php";
                }else{
                    alert(data);
                }
            }
        },
        error: function(xhr, status, thrown){
            console.log("ERROR: " + status + " THROW: " + thrown + " XHR: " + xhr.responseText);
        }
    });
});