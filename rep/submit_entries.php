<?php
  session_start();
  include "../van/php/sql-statements.php";
  $db = new DB;
    if(!isset($_SESSION['username']) || $_SESSION['accountType'] !== 'standard'){
        header("location: ../index.php");
        die();
    }

    $conditions = array(
        'where'=>array(

        )
    );

    $accountId = $db->getRows('tbl_accounts', array('where'=>array('username'=>$_SESSION['username'])));
    $countEntry = $db->getRows('tbl_entries', array('where'=>array('account_id'=>$accountId[0]['account_id']), 'return_type'=>'count'));
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>11th BIDA Award 2017 | Submit Entries</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- fullCalendar 2.2.5-->
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
	   folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- jQuery 2.2.3 -->
  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="scripts/entry.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">

	<!-- Logo -->
	<a href = "../index.php" class="logo">
	  <!-- mini logo for sidebar mini 50x50 pixels -->
	  <span class="logo-mini"><img src="../img/bida_logo.png" style="height: 40px; width: 40px;"></span>
	  <!-- logo for regular state and mobile devices -->
	  <span class="logo-lg"><img src="../img/bida_logo.png" style="height: 40px; width: 40px;"><b>User</b>Dashboard</span>
	</a>

	<!-- Header Navbar -->
	<nav class="navbar navbar-static-top" role="navigation">
	  <!-- Sidebar toggle button-->
	  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
	  </a>
	  <!-- Navbar Right Menu -->
	  <div class="navbar-custom-menu">
		<ul class="nav navbar-nav">
		  <li class="dropdown tasks-menu">
			<!-- Menu Toggle Button -->
			<a href="../index.php">
			  Log Out <i class="fa fa-sign-out"></i>
			</a>
		  </li>
		</ul>
	  </div>
	</nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
	  <!-- Sidebar Menu -->
	  <ul class="sidebar-menu">
          <li class="header" style="font-size: 23px; color: #ddd;">
              Welcome, <?php echo $_SESSION['username']; ?>!
          </li>
		<li class="header">INFORMATIONS</li>
		<!-- Optionally, you can add icons to the links -->
		<li><a href="view_entry.php"><i class="fa fa-search-plus"></i> <span>View Entries</span></a></li>
		<li class="active"><a href="#"><i class="fa fa-check"></i> <span>Submit Entries</span></a></li>
	  </ul>
	  <!-- /.sidebar-menu -->
	</section>
	<!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <h1>
		Entries
		<small>You have submitted <?php if($countEntry < 1){echo " 0";}else{ echo $countEntry;} if($countEntry > 1){ echo " entries.";}else{ echo " entry.";} ?></small>
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Mailbox</li>
	  </ol>
	</section>

	<!-- Main content -->
	<section class="content">
	  <div class="row">
		<div class="col-md-3">
		  <a href="view_entry.php" class="btn btn-primary btn-block margin-bottom">Back to Submitted Entries</a>

		</div>
		<!-- /.col -->
		<div class="col-md-9">
		  <div class="box box-primary">
			<div class="box-header with-border">
			  <h3 class="box-title">Submit a New Entry</h3>
			</div>
			<!-- /.box-header -->
			<div id="myModal" class="box-body">
			  <form id="entryForm">
				<!-- CATEGORY -->
				<div id="page1" class="row" style="text-align: center;">
					<h2 style="text-align: center;">Category</h2>
					<div class="btn-group" data-toggle="buttons" >
					  <label class="btn btn-primary active">
						<input type="radio" name="category" id="food" autocomplete="off" checked value="Food"> Food
					  </label>
					  <label class="btn btn-primary">
						<input type="radio" name="category" id="non-food" autocomplete="off" value="Non-Food"> Non-Food
					  </label>
					  <label class="btn btn-primary">
						<input type="radio" name="category" id="services" autocomplete="off" value="Services"> Services
					  </label>
					  <label class="btn btn-primary">
						<input type="radio" name="category" id="technology" autocomplete="off" value="Technology"> Technology
					  </label>
					</div>

					<div class="box-footer">
					  <div class="pull-right">
                          <button type="button" id="next1" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Next</button>
					  </div>
					</div>
					<!-- /.box-footer -->
				</div>

				<!-- TITLE -->
				<div id="page2" class="row col-xs-6" style="text-align: center; margin-left: 25%;" hidden>
					<h2 >Title</h2>
					Title: <input type="text"  class="form-control" name="title"><br>
					Description: <textarea rows="5" class="form-control" name="description"></textarea><br>

					<div class="box-footer">
					  <div class="pull-left">
						<button class="btn btn-default" id="back2"><i class="glyphicon glyphicon-chevron-left"></i> Previous</button>
					  </div>
					  <div class="pull-right">
						<button type="button" id="next2" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Next</button>
					  </div>
					</div>
				</div>

				<!-- TEAM INFORMATION -->
				<div class="row  col-xs-6" id="page3" style="text-align: center; margin-left: 25%;" hidden>
					<h2>Team Information</h2>
					College/University: <input type="text" class="form-control" name="college"><br>
					College/University's Address: <input type="text"  class="form-control" name="collegeAddress"><br>
					College Dean: <input type="text" class="form-control" name="collegeDean"><br>
					Team Adviser/Coach: <input type="text" class="form-control" name="coach"><br>
					Contact Person: <input type="text" class="form-control" name="contactPerson"><br>
					Landline: <input type="number" class="form-control" name="landline"><br>
					Mobile #: <input type="number" class="form-control" name="mobile"><br>
					E-Mail: <input type="email" class="form-control" name="email"><br>
					<div class="box-footer">
					  <div class="pull-left">
						<button class="btn btn-default" id="back3"><i class="glyphicon glyphicon-chevron-left"></i> Previous</button>
					  </div>
					  <div class="pull-right">
						<button type="button" id="next3" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Next</button>
					  </div>
					</div>
				</div>

				<!-- STUDENTS -->
				<div class="row col-xs-6" id="page4" style="text-align: center; margin-left: 25%;" hidden>
				<p>NOTE: Please select at least 2 alternative contact persons.</p>
					<div id="students">
						<input type="button" id="addStudent" class="btn btn-success" name="addStudent" value="+">
						<input type="button" id="subStudent" class="btn btn-danger" name="subStudent" value="-">
						<div id="stud1" class="well">
							<h3>Student 1</h3>
							First Name:<input type="text" class="form-control input-sm" name="firstname1"><br>
							Last Name:<input type="text" class="form-control input-sm" name="lastname1"><br>
							E-Mail: <input type="email" class="form-control input-sm" name="studentEmail1"><br>
							Mobile: <input type="number" class="form-control input-sm" name="studentMobile1"><br>
							Date of Birth: <input type="date" class="form-control input-sm" name="dob1"><br>
							Gender:
							<label class="radio-inline">
								<input type="radio" name="gender1" value="Male"> Male
							</label>
							<label class="radio-inline">
								<input type="radio" name="gender1" value="Female"> Female
							</label><br>
							<label class="checkbox-inline">
								<input type="checkbox" name="setContact1" class="setContact"> Set as Contact person?
							</label><br>
							<input type="button" class="btn btn-warning btn-xs" id="removeStud1" value="Clear">
						</div>
					</div>
					<div class="box-footer">
					  <div class="pull-left">
						<button class="btn btn-default" id="back4"><i class="glyphicon glyphicon-chevron-left"></i> Previous</button>
					  </div>
					  <div class="pull-right">
						<button type="button" id="next4" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Next</button>
					  </div>
					</div>
				</div>

				<!-- ATTACHMENTS -->
				<div class="row" id="page5" style="text-align: center;" hidden>
				  <div class="form-group">
					<div class="btn btn-default btn-file">
					  <i class="fa fa-paperclip"></i>* Attach Presentation
					  <input type="file" name="presentationUpload">
					</div>
					<div class="btn btn-default btn-file">
					  <i class="fa fa-paperclip"></i>* Attach Document
					  <input type="file" name="documentUpload">
					</div>
					<div class="btn btn-default btn-file">
					  <i class="fa fa-paperclip"></i> Attach Payment proof
					  <input type="file" name="paymentUpload">
					</div>
					<p class="help-block">Max. 32MB<br>* - Required uploads</p>
				  </div>
					<div class="box-footer">
					  <div class="pull-left">
						<button class="btn btn-default" id="back5"><i class="glyphicon glyphicon-chevron-left"></i> Previous</button>
					  </div>
					  <div class="pull-right">
						<button type="button" id="next5" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Next</button>
					  </div>
					</div>
				</div>

				<!-- CERTIFICATIONS -->
				<div class="row" id="page6" style="text-align: center;" hidden>
					<h3>Certifications</h3>
					<h4>A. SCHOOL and/or LOCAL CHAMBER</h4>
					<h5><u>School</u></h5>
					<p>I hereby certify that the entry entitled <i class="certTitle"></i> is the original work of our student/s who is/are currently   enrolled in this school. <br><br>
					<u><i id="certDean" style="text-align: right;"></i></u><br><b>COLLEGE DEAN </b>
					</p><br>
					<h5><u>Or Local Chamber</u></h5>
					<p>The <input type="text" name="chamberName" placeholder="Chamber Name"> is endorsing and/or sponsoring the presentation of the entry of <i class="certTitle"></i>. </p><br>
					<h4>B. SWORN STATEMENT OF THE TEAM</h4>
					<ul style="text-align: left;">
						<li>The business idea was originally created by the undersigned student/s. </li>
						<li>The business idea has not been previously entered in any national competition. </li>
						<li>I am/We are prepared to make up into  my/our  business idea if selected by the Board of Judges and to deliver such business plan to PCCI for final judging.  I/We  understand that if  my/our  business idea is chosen for an Award,  my/our  business plan can be retrieved by me/the group  and the idea made up from it remains  my/our  property.</li>
					</ul>

					<div style="text-align: left;">
						<h3>Students</h3>
						<ul id="studentsList">

						</ul>

						<h3>Coach</h3>
						<p id="certCoach"></p>
					</div>

					<input type="checkbox" name="agreeTerms"> I agree to the terms and conditions.<br>

					<div class="box-footer">
					  <div class="pull-left">
						<button class="btn btn-default" id="back6"><i class="glyphicon glyphicon-chevron-left"></i> Previous</button>
					  </div>
					  <div class="pull-right">
						<button type="button" id="entrySubmit" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> Submit</button>
					  </div>
					</div>

				</div>
			  </form>
			</div>
			<!-- /.box-body -->
		  </div>
		  <!-- /. box -->
		</div>
		<!-- /.col -->
	  </div>
	  <!-- /.row -->
	</section>
	<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
	<!-- To the right -->
	<div class="pull-right hidden-xs">
	  <img src="../img/PCCI_logo.png" style="height: 20px;">
	</div>
	<!-- Default to the left -->
	<strong>Copyright &copy; 2017 <a href="#">FEU - Makati</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->


<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Page Script -->
<script>
  $(function () {
	//Add text editor
	$("#compose-textarea").wysihtml5();
  });
</script>
</body>
</html>
