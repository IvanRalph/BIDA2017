<?php
    session_start();
    if(!isset($_SESSION['username']) || $_SESSION['accountType'] !== 'standard'){
        header("location: ../index.php");
        die();
    }
    include "../van/php/sql-statements.php";


    $db = new DB();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>11th BIDA Award 2017 | View Entries</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    <!-- jQuery 2.2.3 -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">

        <!-- Logo -->
        <a href = "../index.php" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="../img/bida_logo.png" style="height: 40px; width: 40px;"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="../img/bida_logo.png" style="height: 40px; width: 40px;"><b>User</b>Dashboard</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown tasks-menu">
                        <!-- Menu Toggle Button -->
                        <a href="../index.php">
                            Log Out <i class="fa fa-sign-out"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header" style="font-size: 23px; color: #ddd;">
                    Welcome, <?php echo $_SESSION['username']; ?>!
                </li>
                <li class="header">INFORMATIONS</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="active"><a href="view_entry.php"><i class="fa fa-search-plus"></i> <span>View Entries</span></a></li>
                <li><a href="submit_entries.php"><i class="fa fa-check"></i> <span>Submit Entries</span></a></li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
    <?php
        $user = $db->getRows('tbl_accounts', array('where'=>array('username'=>$_SESSION['username'])));
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Account Settings
            </h1>
            <br>
        </section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <form class="form-horizontal" id="accountSettings" style="border-right: 1px solid #d6d6d6;">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Username</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $user[0]['username']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">School Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="schoolName" placeholder="School Name" value="<?php echo $user[0]['school_name']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">School Address</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="schoolAddress" placeholder="School Address" value="<?php echo $user[0]['school_address']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">School Contact No.</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="schoolNumber" placeholder="School Contact No." value="<?php echo $user[0]['contact_info']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">School E-Mail</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="schoolEmail" placeholder="School E-Mail" value="<?php echo $user[0]['school_email']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">First Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="firstName" placeholder="First Name" value="<?php echo $user[0]['first_name']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Last Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="lastName" placeholder="Last Name" value="<?php echo $user[0]['last_name']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-lg-6">
                    <form class="form-horizontal" id="changePassForm2">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Old Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="oldPassword" placeholder="Old Password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">New Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="newPassword" placeholder="New Password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Confirm New Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="conNewPassword" placeholder="Confirm New Password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            <img src="../img/PCCI_logo.png" style="height: 20px;">
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2017 <a href="#">FEU - Makati</a>.</strong> All rights reserved.
    </footer>
</div>
<!-- ./wrapper -->

<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="scripts/entry.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- Page Script -->
<script>
    $(function () {
        //Enable iCheck plugin for checkboxes
        //iCheck for checkbox and radio inputs
        $('.mailbox-messages input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        });

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
            var clicks = $(this).data('clicks');
            if (clicks) {
                //Uncheck all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
            } else {
                //Check all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("check");
                $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
            }
            $(this).data("clicks", !clicks);
        });

        //Handle starring for glyphicon and font awesome
        $(".mailbox-star").click(function (e) {
            e.preventDefault();
            //detect type
            var $this = $(this).find("a > i");
            var glyph = $this.hasClass("glyphicon");
            var fa = $this.hasClass("fa");

            //Switch states
            if (glyph) {
                $this.toggleClass("glyphicon-star");
                $this.toggleClass("glyphicon-star-empty");
            }

            if (fa) {
                $this.toggleClass("fa-star");
                $this.toggleClass("fa-star-o");
            }
        });
    });
</script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
