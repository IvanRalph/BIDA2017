<?php
    if (session_status() === PHP_SESSION_ACTIVE) {
        session_destroy();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>11th Business Idea and Development Award 2017</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">
    
    <link href="vendor/bootstrap/css/magnific-popup/magnific-popup.css">
    <script src="vendor/jquery/jquery.min.js"></script>

    <script type="text/javascript" src="van/scripts/register.js"></script>
    <script type="text/javascript" src="van/scripts/login.js"></script>
	

    <style>
	
    .navbar-toggler {
        z-index: 1;
    }
    
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }
    </style>
    
    <script>
    function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 120,
                nav = document.querySelector("nav");
            if (distanceY > shrinkOn) {
                classie.add(nav,"smaller");
            } else {
                if (classie.has(nav,"smaller")) {
                    classie.remove(nav,"smaller");
                }
            }
        });
    }
    window.onload = init();
</script>

</head>

<body id="page-top">

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span><span aria-hidden="true"><img src="img/bida_logo.png" height="50px;" width="50px;" style = "float: right; margin-left: 260px;"></span></button>
          </div>
          <div class="modal-body">
            <form id="reg_form">
                <label>Username: <i style="color: red">*</i></label> <br>
 				<input type="text" class = "form-control" style="width: 50%" name="username">
               	<br>
               	
                <label>Password: <i style="color: red">*</i></label> <br>
                <input type="password" class = "form-control" style="width: 50%" name="password"> 
                <br>
                
                <label>Confirm Password: <i style="color: red">*</i></label> <br>
                <input type="password" class = "form-control" style="width: 50%" name="confirmPass"> 
                <br><br>
                
                <div id="schoolDetails">
                    <h2>School Details</h2>
                    <br>
                    
                    <label>Name of School: <i style="color: red">*</i></label> <br>
                	<input type="text" class = "form-control" style="width: 50%" name="schoolName"> 
                	<br>
                   
                    <label>School Address: <i style="color: red">*</i></label> <br>
                	<input type="text" class = "form-control" style="width: 50%" name="schoolAddress"> 
                	<br>
                   
                    <label>Contact Number: <i style="color: red">*</i></label> <br>
                	<input type="text" class = "form-control" style="width: 50%" name="contactInfo"> 
                	<br>
                   
                    <label>E-Mail Address: <i style="color: red">*</i></label> <br>
                	<input type="email" class = "form-control" style="width: 50%" name="schoolEmail"> 
                	<br><br>
                	
                </div>
                <div id="personalDetails">
                    <h2>Personal Details</h2>
                    <br>
                    
                    <label>Last Name: <i style="color: red">*</i></label> <br>
                	<input type="text" class = "form-control" style="width: 50%" name="lastName"> 
                	<br>
                   
                    <label>First Name: <i style="color: red">*</i></label> <br>
                	<input type="text" class = "form-control" style="width: 50%" name="firstName"> 
                	<br>
                   
                    <label>Middle Name: <i style="color: red">*</i></label> <br>
                	<input type="text" class = "form-control" style="width: 50%" name="middleName"> 
                	<br>
                   
                   	<label>Gender: <i style="color: red">*</i></label> <br>
                   	
                   	<div class="col-md-4">
                    	<ul class="list-inline quicklinks">
                        	<li class="list-inline-item">
                            	<input type="radio" name="gender" value="male"> &nbsp;&nbsp; Male
                        	</li>
                        	<li class="list-inline-item">
                            	<input type="radio" name="gender" value="female"> &nbsp;&nbsp; Female
                        	</li>
                        
                    	</ul>
                	</div>
                	
                	<br> 
                   
                   	<label>Date of Birth: <i style="color: red">*</i></label> <br>
                	<input type="date" class = "form-control" style="width: 50%" name="dob"> 
                	<br>
                  	
                  	<label>E-Mail Address: <i style="color: red">*</i></label> <br>
                	<input type="email" class = "form-control" style="width: 50%" name="personalEmail"> 
                	<br>
                  	
                  	<label>Contact Number: <i style="color: red">*</i></label> <br>
                	<input type="text" class = "form-control" style="width: 50%" name="contactNumber"> 
                	<br>
                  	
                  	<label>Department: <i style="color: red">*</i></label> <br>
                	<input type="text" class = "form-control" style="width: 50%" name="department"> 
                	<br>
                  	
                  	<label>School Head: <i style="color: red">*</i></label> <br>
                	<input type="text" class = "form-control" style="width: 50%" name="schoolHead"> 
                	<br>
                  	
                  	<label>Position: <i style="color: red">*</i></label> <br>
                	<input type="text" class = "form-control" style="width: 50%" name="position"> 
                	<br>
                
                </div>

                <i style="color: red">* - required fields</i><br>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="button" name="submit" class="btn btn-primary" value="SUBMIT" onclick="return reg_validation()">
          </div>
        </div>
      </div>
    </div>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse" id="mainNav">
        <div class="container">
        <img id="logo"/> 
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="#page-top">
            <br>
           </a>  
            <div class="collapse navbar-collapse" id="navbarResponsive">
                  <ul class="navbar-nav" style="margin-top: 7px;">
                   
                     <li class="nav-item">
                        <a class="nav-link" href="index.php"><strong>Home</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about"><strong>About</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#portfolio"><strong>Gallery</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="html/downloads.html"><strong>Downloads</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact"><strong>Contact Us</strong></a>
                    </li>
                    
                </ul>
                
                
                <ul class="navbar-nav ml-auto" style="margin-top: 15px;">
                   <li class="nav-item">
				<form id="login_form" class="navbar-nav ml-auto" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" style="height: 35px; width: 130px;" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" style="height: 35px; width: 130px;" name="password" placeholder="Password">
                        
                        <center>
                        	<a href="#" data-toggle="modal" data-target="#myModal"><span class = "register"><b><u>Create an Account</u></b></span></a>
                        </center>
                    </div>
                    
                    <button type="button" style="height: 35px;" class = "form-group btn btn-success" onclick="login_validation()">Sign In</button>
                    
                     
                </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
        <div class="container">
            <div class="intro-text text-center">
               <center><img class = "front_logo" src="img/bida_logo.png"></center>
               
               <h2 class="intro-theme"><b>“Creating Innovative Mindsets among Young Entrepreneurs”</b></h2>
                <br><br>
                <a href="#categories" class="btn btn-circle btn-xl"><i class="fa fa-2x fa-angle-double-down"></i></a>
                
            </div>
        </div>
    </header>
    
     <!-- About -->
    <section id="about" class="bg-gallery">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-gray-dark">About</h2>
                    <h3 class="section-subheading text-muted">The Search for the Best Business Idea and Development Award (BIDA) 2017</h3>
                </div>
            </div>
            
            <div class="row">
                   	<div class="col-md-6">
                   	
                   	<img src="img/about.png" style = "margin-left: -120px;" height = "630px" width="700px;">
                  
                    
                </div>
                  	<div class="col-md-6">
                    
                    <p class="text-muted" style = "text-align: justify">
                   		The Philippine Chamber of Commerce and Industry (PCCI)-Education Committee seeks to contribute to the development of a globally competitive manpower-based and highly dynamic and successful entrepreneurs through training and education activities. Its primary role is to initiate  and or participate in programs and activities that would promote human resource development aligned with the requirements of today's business environment.
                  	</p>
                  
                  <p class="text-muted" style = "text-align: justify">
                   		One of its mission is to develop and promote creative Filipino entrepreneurs among students. In the year 1995, the PCCI together with the Planters Bank of the Philippines and Department of Trade and Industry (DTI) launched the 1st BIDA. In the year 2007, under the leadership of the PCCI Vice President for Education, Dr. Eduardo Gutierrez-Ong, the committee spearheaded the Search with a distinct competition where the students were recognized for their ideas in entrepreneurship emphasizing the attributes of a Filipino. 
                  </p>
                  
                  <p class="text-muted" style = "text-align: justify">
                   		With this in view, a project dubbed as “Business Idea and Development Award” or BIDA, was conceptualized to bring out the entrepreneurial spirit and creativeness of the students. 
                  </p>
                  
                  <p class="text-muted" style = "text-align: justify">
                   		To date, Dr. Ong, Chairman for Education, continues to pursue the commitment of PCCI to play a big role in the creation of wealth in the countryside through agriculture, manufacturing and services. Recognizing the role of student entrepreneurs in nation building, the Education Committee is embarking the 9th Search for the Best Business Idea and Development Award (BIDA) competition 2017. 
                  </p>
               
                  
                </div>
                   	
              </div>
            </div>
    </section>
    
    <!-- Objectives -->
    
    <section class ="bg-faded" id = "objectives" style="background-image: url(img/backgrounds/objective-bg.jpg)">
        <div class="container">
          <div class="row">
                <div class="col-lg-12 col-md-2 col-sm-6 text-center">
                    <h2 class="section-heading">Objectives</h2>
                </div>
            </div>
           <div class="row">
                <div class="col-md-6 col-sm-6 text-center">
                    <div class="portfolio-link" data-toggle="modal">
                        <img class="img-fluid d-block mx-auto" style = "height: 200px; width: 200px;" src="img/objectives/A.png" alt="">
                    </div>
                    <p class="text-muted">Recognize creative or innovative products or services using available local, indigenous or recycled materials</p>
                </div>
                <div class="col-md-6 col-sm-6 text-center">
                    <div class="portfolio-link" data-toggle="modal">
                        <img class="img-fluid d-block mx-auto" style = "height: 200px; width: 200px;" src="img/objectives/B.png" alt="">
                    </div>
                    <p class="text-muted">Develop entrepreneurial culture among Filipino youth</p>
                </div>
                <div class="col-md-6 col-sm-6 text-center">
                    <div class="portfolio-link" data-toggle="modal">
                        <img class="img-fluid d-block mx-auto" style = "height: 200px; width: 200px;"  src="img/objectives/C.png" alt="">
                    </div>
                    <p class="text-muted">Promote and develop a sense of nationalism among Filipino youth through product or service development</p>
                </div>
                <div class="col-md-6 col-sm-6 text-center">
                    <div class="portfolio-link" data-toggle="modal">
                        <img class="img-fluid d-block mx-auto" style = "height: 200px; width: 200px;" src="img/objectives/D.png" alt="">
                    </div>
                     <p class="text-muted">Promote entrepreneurship as major component in our Economic Sustainable Development (ESD).</p>
                </div>
            </div>
        </div>
    </section>
    
     <!-- Categories -->
    <section class ="bg-primary" id = "categories">
        <div class="container">
          <div class="row">
                <div class="col-lg-12 col-md-2 col-sm-6 text-center">
                    <h2 class="section-heading">Award Categories</h2>
                    <h3 class="section-subheading">click the icon to know more</h3>
                </div>
            </div>
           <div class="row">
                <div class="col-md-3 col-sm-6 text-center">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                        <img class="img-fluid d-block mx-auto" src="img/categories/foods.png" alt="">
                    </div>
                    <h1>Food</h1>
                   
                </div>
                <div class="col-md-3 col-sm-6 text-center">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                        <img class="img-fluid d-block mx-auto" src="img/categories/non-food.png" alt="">
                    </div>
                    <h1>Non-Food</h1>
                  
                </div>
                <div class="col-md-3 col-sm-6 text-center">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                        <img class="img-fluid d-block mx-auto" src="img/categories/services.png" alt="">
                    </div>
                    <h1>Services</h1>
               
                </div>
                <div class="col-md-3 col-sm-6 text-center">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                        <img class="img-fluid d-block mx-auto" src="img/categories/tech.png" alt="">
                    </div>
                     <h1>Tech</h1>
                   
                </div>
            </div>
        </div>
    </section>
    
  <!-- Mechanics --> 
    <section style="background-image: url(img/backgrounds/objectives-bg.jpg)">
        <div class="container">
            <div class="section-heading text-center">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Mechanics</h2>
                </div>
                <br><br>
            </div>
			
			<div class="row">
                <div class="col-md-6">
                   <div class="feature-item">
                        <h3 style="color: #248475; font-weight: 200;">
                                    <button type="button" class="btn-outline-success btn-circle" >1</button> Search for the best BIDA</h3>
                                    <small class="text-muted" style = "text-align: justify">The search for the best Business Idea and Development Award (BIDA) is open to any group of 3 to 5 college students enrolled in business education, science and technology and related programs online</small>
                                    <br><br>
                                </div>
					
					<div class="feature-item">
                                	<h3 style="color: #248475; font-weight: 200;">
                                    <button type="button" class="btn-outline-success btn-circle" onClick="updateTheInfo(1)">2</button> Endorsement of Entries</h3>
                                    <small class="text-muted" style = "text-align: justify">All entries must be endorsed by their respective college/university Dean or any local chamber affiliated with PCCI. All colleges and universities are allowed to send a maximum of two entries per category.</small>
                                    <br><br>
                                </div>
					
					<div class="feature-item">
                                	<h3 style="color: #248475; font-weight: 200;">
                                    <button type="button" class="btn-outline-success btn-circle" onClick="updateTheInfo(1)">3</button> Entry Forms</h3>
                                    <small class="text-muted" style = "text-align: justify">Entry forms together with the Business Plan Format can be downloaded at <strong><u>www.bida.com</u></strong> or secured from PCCI or in any participating agencies.</small>
                                    <br><br>
                                </div>
					
					<div class="feature-item">
                                	<h3 style="color: #248475; font-weight: 200;">
                                    <button type="button" class="btn-outline-success btn-circle" onClick="updateTheInfo(1)">4</button> Submission of BIDA Form</h3>
                                    <small class="text-muted" style = "text-align: justify">All participating groups shall submit the fully accomplished Official BIDA Entry Form and Executive Summary of their business plans to PCCI <strong>not later than August 30, 2017.</strong></small>
                                    <br><br>
                                </div>
					
					<div class="feature-item">
                                	<h3 style="color: #248475; font-weight: 200;">
                                    <button type="button" class="btn-outline-success btn-circle" onClick="updateTheInfo(1)">5</button> Choosing of Top 10 Finalists</h3>
                                    <small class="text-muted" style = "text-align: justify">Ten (10) group finalists per category will be determined by a panel of judges from the PCCI Education sub-committee on BIDA. These finalists will be ranked from highest to lowest.</small>
                                    <br><br>
                                </div>
                  
                 
                   
                </div>
                <div class="col-md-6">
                     <div class="feature-item">
                                	<h3 style="color: #248475; font-weight: 200;">
                                    <button type="button" class="btn-outline-success btn-circle" onClick="updateTheInfo(1)">6</button> 15-minute Oral Presentation</h3>
                                    <small class="text-muted" style = "text-align: justify">The business plan shall be submitted together with the product or service prototype to the PCCI following the BIDA format. A product/service prototype is optional except for food category. Finalists will be requested to make a 15-minute oral presentation to a panel of judges.</small>
                                    <br><br>
                                </div>
					
					<div class="feature-item">
                                	<h3 style="color: #248475; font-weight: 200;">
                                    <button type="button" class="btn-outline-success btn-circle" onClick="updateTheInfo(1)">7</button> Codename may be Used</h3>
                                    <small class="text-muted" style = "text-align: justify">There will be no mentioning of school name on business plans or presentation materials as well as wearing of uniforms during the preliminary screening or oral presentation. A codename may be used and indicated in the business plan or presentation materials.</small>
                                    <br><br>
                                </div>
					
					<div class="feature-item">
                                	<h3 style="color: #248475; font-weight: 200;">
                                    <button type="button" class="btn-outline-success btn-circle" onClick="updateTheInfo(1)">8</button> The Business Plan</h3>
                                    <small class="text-muted" style = "text-align: justify">The idea in the submitted business plan must not have been previously entered in national competitions.</small>
                                    <br><br>
                                </div>
					
					<div class="feature-item">
                                	<h3 style="color: #248475; font-weight: 200;">
                                    <button type="button" class="btn-outline-success btn-circle" onClick="updateTheInfo(1)">9</button> Winning Entries for BIDA</h3>
                                    <small class="text-muted" style = "text-align: justify">The winning entries for BIDA may be considered for adoption by Local Chambers or investors* in the implementation stage until the realization of the project (* to be invited as observers during the final judging).</small>
                                    <br><br>
                                </div>
					
					<div class="feature-item">
                                	<h3 style="color: #248475; font-weight: 200;">
                                    <button type="button" class="btn-outline-success btn-circle" onClick="updateTheInfo(1)">10</button> Winning Entries for BIDA</h3>
                                    <small class="text-muted" style = "text-align: justify">All prototypes including the ownership of ideas will remain as the property of the students and college/university represented.</small>
                                    <br><br>
                                </div>
					
                </div>
                
            </div>
           
			</div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>

    
    <!-- Portfolio Grid -->
    <section class="bg-faded" id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Gallery</h2>
                </div>
            </div>
            
            <br><br>
            <div class="row">
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid" src="img/bida logo/2016.jpg" alt="">
                    </div>
                    <div class="portfolio-caption">
                        <h4>BIDA 2016</h4>
                        <p class="text-muted">10<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid" src="img/bida logo/2015.jpg" alt="">
                    </div>
                    <div class="portfolio-caption">
                        <h4>BIDA 2015</h4>
                        <p class="text-muted">9<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal7">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid" src="img/bida logo/2014.jpg" alt="">
                    </div>
                    <div class="portfolio-caption">
                         <h4>BIDA 2014</h4>
                        <p class="text-muted">8<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal8">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid" src="img/bida logo/2013.jpg" alt="">
                    </div>
                    <div class="portfolio-caption">
                         <h4>BIDA 2013</h4>
                        <p class="text-muted">7<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal9">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid" src="img/bida logo/2012.jpg" alt="">
                    </div>
                    <div class="portfolio-caption">
                         <h4>BIDA 2012</h4>
                        <p class="text-muted">6<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal10">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid" src="img/bida logo/2011.jpg" alt="">
                    </div>
                    <div class="portfolio-caption">
                        <h4>BIDA 2011</h4>
                        <p class="text-muted">5<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal11">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid" src="img/bida logo/2010.jpg" alt="">
                    </div>
                    <div class="portfolio-caption">
                         <h4>BIDA 2010</h4>
                        <p class="text-muted">4<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal12">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid" src="img/bida logo/2009.jpg" alt="">
                    </div>
                    <div class="portfolio-caption">
                         <h4>BIDA 2009</h4>
                        <p class="text-muted">3<sup>rd</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <div class="portfolio-link" data-toggle="modal" href="#portfolioModal13">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid" src="img/bida logo/2008.jpg" alt="">
                    </div>
                    <div class="portfolio-caption">
                        <h4>BIDA 2008</h4>
                        <p class="text-muted">2<sup>nd</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- About -->
    <section id="phases" class = "bg-gallery">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Competition Phases</h2>
                </div>
                
                <br><br><br><br>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="rounded-circle img-fluid" src="img/about/1.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 style="color: #005395">August 2017</h4>
                                    <h4 class="subheading">Submission of Entries</h4>
                                </div>
                                
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="rounded-circle img-fluid" src="img/about/2.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 style="color: #72006f">September 2017</h4>
                                    <h4 class="subheading">Preliminary Screening</h4>
                                </div>
                                
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="rounded-circle img-fluid" src="img/about/3.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 style="color: #874400">September 2017</h4>
                                    <h4 class="subheading">Submission of Complete Paper</h4>
                                </div>
                                
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="rounded-circle img-fluid" src="img/about/4.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 style="color: #8c9400">October 2017</h4>
                                    <h4 class="subheading">Final Judging</h4>
                                </div>
                                
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="rounded-circle img-fluid" src="img/about/5.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 style="color: #4a7501">October 2017</h4>
                                    <h4 class="subheading">Awarding Ceremonies</h4>
                                </div>
                               
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Be
                                    <br> A
                                    <br>Part!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
    <section class ="bg-faded" style="padding: 30px;">
        <div class="container">
          <div class="row">
                <div class="col-lg-12 col-md-2 col-sm-6 text-center">
                    <h4 class="section-heading">Deadline of Submission of Entries is on 31 August 2017, Thursday</h4>
                </div>
            </div>
    </section>
    
       <!-- Participation Fee -->
        <section style="padding: 90px;">
        <div class="container">
          <div class="row">
                <div class="col-lg-12 col-md-2 col-sm-6 text-center">
                    <h2 class="section-heading">Participation Fee</h2>
                </div>
            </div>
           
           <div class="row">
                <div class="col-lg-12">
                    <div class="team-member">
                       
                       <a><img class="ml-auto" src="img/payment.png" style = "height: 210px; width: 250px;" alt=""></a>
                       
                        
                        <p class="text-muted">There will be a <strong>participation fee of Two Thousand Two Hundred Forty Pesos (P2,240.00)</strong> inclusive of 12% VAT per entry payable to the Philippine Chamber of Commerce and Industry upon <strong>submission of Official BIDA Entry Form with Executive Summary.</strong> </p>
                        
                    </div>
                </div>
                
        </div>
        
    </section>
     
        <!-- Prizes -->
        <section class ="bg-faded" id = "prizes" style="padding: 90px;">
        <div class="container">
          <div class="row">
                <div class="col-lg-12 col-md-2 col-sm-6 text-center">
                    <h2 class="section-heading">Prizes</h2>
                </div>
            </div>
           
           <div class="row">
                <div class="col-lg-12">
                    <div class="team-member">
                        <a><img class="ml-auto" src="img/prize.png" alt=""></a>
                        <p class="text-muted">There will be one <strong>(1) grand winning team for each category</strong> and shall receive <strong>P50,000.00</strong> cash prize and plaque. <br> Other winning entries <strong>(2nd and 3rd) may also receive prizes.</strong></p>
                        
                    </div>
                </div>
                
        </div>
        
    </section>
    
     <!-- Winners 
        <section id = "winners" style="padding: 40px;">
        <div class="container">
          <div class="row">
                <div class="col-lg-12 col-md-2 col-sm-6 text-center">
                    <h2 class="section-heading">Announcement of Winners</h2>
                </div>
            </div>
           
           <div class="row">
                <div class="col-lg-12">
                    <div class="team-member">
                        
                        <p class="text-muted">The awards shall be presented to the winners during the 43rd Philippine Business Conference in October 2017 at the Manila Hotel. </p>
                        
                    </div>
                </div>
                
        </div>
        
    </section>
   -->

    <!-- Team 
    <section class="bg-faded" id="sponsors">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading" style="color: green">Our Sponsors</h2>
                    
                </div>
                
            </div>
                
                
                <br><br>
            </div>
            <div class="row">
                <div class="col-sm-4 offset-lg-2">
                    <div class="team-member">
                        <a style = "color: black;"  href="http://www.philexport.ph"><img class="ml-auto" src="img/sponsors/no-circle/philexport.png" alt=""></a>
                        <a style = "color: black;"  href="http://www.philexport.ph"><h4>Philippine Exporters Confederation, Inc.</h4></a>
                        
                    </div>
                </div>
                 <div class="col-sm-4">
                    <div class="team-member">
                        <a style = "color: black;" href="http://www.calatacorp.com"><img class="ml-auto" src="img/sponsors/no-circle/calata.png" alt=""></a>
						<a style = "color: black;" href="http://www.calatacorp.com"><h4>Calata Corporation</h4></a>
                        
                    </div>
                </div>
           
        </div>
            </div>
            
                
    </section>
-->
   

    <!-- Contact -->
    <section id="contact" style = "padding: 90px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-gray-dark">Contact Us</h2>
                </div>
            </div>
            
            <br><br>
            <div class="row">
                <div class="col-md-6">
                   <p class="text-info">Philippine Chamber of Commerce and Industry (PCCI)</p>
                   	<p class="text-muted">
                   		3/F Commerce and Industry Plaza <br>
						1030 Campus Ave. cor. Park Ave., McKinley Town Center <br>
						Fort Bonifacio, Taguig City 1634 <br>
                  </p>
                  
                 
                   
                </div>
                <div class="col-md-6">
                     <p class="text-info">c/o Ms. Juliet Suñga-Espino</p>
                   	<p class="text-muted">
                   		Telephone Number: 846-8196 local 108 <br>
						Fax Number: 846-8619 <br>
						Email: <u><a style = "color: #0A77BE" href="mailto:your-email@your-domain.com">juliet.sunga@philippinechamber.com </a></u> <br>
                  </p>
                </div>
                
            </div>
            
            <div class="row">
            	<div class="col-md-6">
                   <h3 class="text-info">How to get there</h3>
                   <p class="text-info">To PCCI by private/service:</p>
                   
                   	<p class="text-muted">
                   		<strong>From EDSA Ayala</strong> <br>
          				Take McKinley Road and turn right to Lawton Ave; <br>
          				Turn left to Upper McKinley Hill Road; <br>
          				Turn right to Park Ave and Campus Ave. <br>
                  	</p>
                  	
                  	<p class="text-muted">
                   		<strong>From EDSA Magallanes</strong> <br>
          				    Take Don Chino Roces Ave. Extension; <br>
          					Turn left to Lawton Ave.; <br>
          					Turn right to Upper McKinley Hill Road; <br>
          					Turn right to Park Ave. and Campus Ave. <br>
                  	</p>
                  	
                  	<p class="text-muted">
                   		<strong>From C5</strong> <br>
          				     Go straight ahead, no need to pass by at  BGC <br> or Market! Market! 
          				     Turn and stay at the right lane <br> approaching  Petron/Jollibee (service road). <br>
          					 Turn right to McKinley Hill, Upper McKinley Road <br>
          					 Turn left to Park Ave.; turn right to Campus Ave. <br>
                  	</p>
                  	
                  	<p class="text-muted">
                   		<strong>Parking spaces are available in front and at the back of Venice Piazza.</strong>
                  	</p>
                  	
                  	
                
                </div>
                
                <div class="col-md-6">
                     <p class="text-muted">
                     <p class="text-info">To PCCI by private/service:</p>
                   		Take the Citylink bus or FX shuttle/van service stationed at the San Lorenzo Place in EDSA Magallanes (near MRT Station) and/or SM Makati Car Park/Terminal and get down at the Commerce and Industry Plaza (CIP)
                  	</p>
                  	
                  	<p class="text-muted">
                   		<strong>Fare: Php 25.00</strong>
                  	</p>
                  	
                  	<img src="img/PCCI_logo2.png" style="margin-top: 60px; height: 100%;">
                  	
                </div>
            </div>
            
        </div>
    </section>
    
    <!-- Map -->
    
    <section class = "map" id="map" style="padding: 1px;">
        
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3862.1324852964394!2d121.05020651467986!3d14.534413482592146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c8c115b421eb%3A0x9cca0b92ef9bd04c!2sPhilippine+Chamber+of+Commerce+and+Industry+-+PCCI+Secretariat+Office!5e0!3m2!1sen!2sph!4v1496823414174" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
         
    </section>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span style="color: #468698; font-weight: 700;" class="copyright">
                    <img src="img/PCCI_logo.png" height="50px;"></p></span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        
                        <li class="list-inline-item">
                            <a href="https://www.facebook.com/BIDAAward/"><i class="fa fa-facebook"></i></a>
                        </li>
                       
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li class="list-inline-item">
                            Copyright &copy; <strong>FEU-Makati 2017
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- Portfolio Modals -->

    <!-- Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="modal-body">
                               <h2><img src="img/categories/foods.png" style = "width: 200px;"> FOOD</h2>
                               
                               <br>
                               
                               <img src="img/categories/sample/food.jpg" width="100%;">
                
        					</div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
    </div>

    <!-- Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="modal-body">
                               <h2><img src="img/categories/non-food.png" style = "width: 200px;"> NON-FOOD</h2>
                               
                               <br>
                               
                               <img src="img/categories/sample/non-food.jpg" width="100%;">
                
        					</div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
    </div>
    
    <!-- Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="modal-body">
                               <h2><img src="img/categories/services.png" style = "width: 200px;"> SERVICES</h2>
                               
                               <br>
                               
                               <img src="img/categories/sample/service.jpg" width="100%;">
                
                           
        					</div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
    </div>

    <!-- Modal 4 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="modal-body">
                               <h2><img src="img/categories/tech.png" style = "width: 200px;"> TECH</h2>
                               
                               <br>
                               
                               <img src="img/categories/sample/tech.jpg" width="100%;">
                
        					</div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
    </div>
    
    <!-- 2016 -->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body">
                               <div class="portfolio-caption text-center">
                        <h4>BIDA 2016</h4>
                        
                    </div>
                              <br><br>
                              
                              <iframe src="https://www.icloud.com/keynote/0RkPft6OVBbo5ivj76Mqhfe4w?embed=true" width="640" height="500" frameborder="0" allowfullscreen="1" referrer="no-referrer"></iframe>
                         
                      <br><br><br>
                       
                        <div class="portfolio-caption text-center">
                        <p class="text-muted">10<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
</div>
   
    <!-- 2015 -->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body">
                               <div class="portfolio-caption text-center">
                        <h4>BIDA 2015</h4>
                        
                    </div>
                              <br><br>
                              
                              <iframe src="https://www.icloud.com/keynote/0Q9wxzjd-CA-DOnFCcTY94jtA?embed=true" width="640" height="500" frameborder="0" allowfullscreen="1" referrer="no-referrer"></iframe>
                         
                      <br><br><br>
                       
                        <div class="portfolio-caption text-center">
                        <p class="text-muted">9<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
</div>
    
    <!-- 2014 -->
    <div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body">
                               <div class="portfolio-caption text-center">
                        <h4>BIDA 2014</h4>
                        
                    </div>
                              <br><br>
                              
                             <iframe src="https://www.icloud.com/keynote/09K6Th2DlDkHmTCg2WdStAcCw?embed=true" width="640" height="500" frameborder="0" allowfullscreen="1" referrer="no-referrer"></iframe>
                         
                      <br><br><br>
                       
                        <div class="portfolio-caption text-center">
                        <p class="text-muted">8<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
</div>
  
  
<!-- 2013 -->
    <div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body">
                               <div class="portfolio-caption text-center">
                        <h4>BIDA 2013</h4>
                        
                    </div>
                              <br><br>
                              
                              <iframe src="https://www.icloud.com/keynote/0ol68PwmxNNRb6XXVnvv_dJdg?embed=true" width="640" height="500" frameborder="0" allowfullscreen="1" referrer="no-referrer"></iframe>
                         
                      <br><br><br>
                       
                        <div class="portfolio-caption text-center">
                        <p class="text-muted">7<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
</div>
  
  <!-- 2012 -->
    <div class="portfolio-modal modal fade" id="portfolioModal9" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body">
                               <div class="portfolio-caption text-center">
                        <h4>BIDA 2012</h4>
                        
                    </div>
                              <br><br>
                              
                              <iframe src="https://www.icloud.com/keynote/0nyi4TCYCSjB-yMZOOimN9lVA?embed=true" width="640" height="500" frameborder="0" allowfullscreen="1" referrer="no-referrer"></iframe>
                         
                      <br><br><br>
                       
                        <div class="portfolio-caption text-center">
                        <p class="text-muted">6<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
</div>
   
   <!-- 2011 -->
    <div class="portfolio-modal modal fade" id="portfolioModal10" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body">
                               <div class="portfolio-caption text-center">
                        <h4>BIDA 2011</h4>
                        
                    </div>
                              <br><br>
                              
                              <iframe src="https://www.icloud.com/keynote/0QStHR_4X-yn9wrPXkTmyEj_A?embed=true" width="640" height="500" frameborder="0" allowfullscreen="1" referrer="no-referrer"></iframe>
                         
                      <br><br><br>
                       
                        <div class="portfolio-caption text-center">
                        <p class="text-muted">5<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
</div>
  
  <!-- 2010 -->
    <div class="portfolio-modal modal fade" id="portfolioModal11" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body">
                               <div class="portfolio-caption text-center">
                        <h4>BIDA 2010</h4>
                        
                    </div>
                              <br><br>
                              
                              <iframe src="https://www.icloud.com/keynote/0bAtiQrzoPORI_PL3WAvvSEuA?embed=true" width="640" height="500" frameborder="0" allowfullscreen="1" referrer="no-referrer"></iframe>
                         
                      <br><br><br>
                       
                        <div class="portfolio-caption text-center">
                        <p class="text-muted">4<sup>th</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
</div>
  
  <!-- 2009 -->
    <div class="portfolio-modal modal fade" id="portfolioModal12" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body">
                               <div class="portfolio-caption text-center">
                        <h4>BIDA 2009</h4>
                        
                    </div>
                              <br><br>
                              
                             <iframe src="https://www.icloud.com/keynote/0Kv3Nclma26IeeKjmlS7qKKpA?embed=true" width="640" height="500" frameborder="0" allowfullscreen="1" referrer="no-referrer"></iframe>
                         
                      <br><br><br>
                       
                        <div class="portfolio-caption text-center">
                        <p class="text-muted">3<sup>rd</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
</div>
  
  <!-- 2008 -->
    <div class="portfolio-modal modal fade" id="portfolioModal13" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body">
                               <div class="portfolio-caption text-center">
                        <h4>BIDA 2008</h4>
                        
                    </div>
                              <br><br>
                              
                              <iframe src="https://www.icloud.com/keynote/077obvuI91Lr2-QSid1hnwc_Q?embed=true" width="640" height="500" frameborder="0" allowfullscreen="1" referrer="no-referrer"></iframe>
                         
                      <br><br><br>
                       
                        <div class="portfolio-caption text-center">
                        <p class="text-muted">2<sup>nd</sup> Search for the Best Business Idea and Development Award</p>
                    </div>
                       </div>
                   </div>
              </div>
           </div>
        </div>
     </div>
</div>
   
    <!-- Bootstrap core JavaScript -->
    
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts -->
    <script src="js/agency.min.js"></script>
    <script src="js/classie.js"></script>
    
    <!-- Pop up Modal-->
    <link href="vendor/bootstrap/css/magnific-popup/jquery.magnific-popup.min.js">

</body>

</html>
