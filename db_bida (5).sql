-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2017 at 09:29 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bida`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_accounts`
--

CREATE TABLE `tbl_accounts` (
  `account_id` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `school_name` varchar(50) NOT NULL,
  `school_address` longtext NOT NULL,
  `contact_info` varchar(10) NOT NULL,
  `school_email` varchar(30) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `first_name` varchar(15) NOT NULL,
  `middle_name` varchar(15) DEFAULT NULL,
  `gender` varchar(6) NOT NULL,
  `birth_date` date NOT NULL,
  `personal_email` varchar(30) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `department` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `school_head` varchar(30) NOT NULL,
  `account_type` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_accounts`
--

INSERT INTO `tbl_accounts` (`account_id`, `username`, `password`, `school_name`, `school_address`, `contact_info`, `school_email`, `last_name`, `first_name`, `middle_name`, `gender`, `birth_date`, `personal_email`, `contact_number`, `department`, `position`, `school_head`, `account_type`) VALUES
(1, 'ivanralph', 'e685b871e786ad13431a3722d5e3945a', 'asdasdlkj', 'asdkljasdlkj', 'askldjaslk', 'asd@asd', 'Vitto', 'Ivan Ralph', 'Garcia', 'male', '2017-06-17', 'asd@asd', '1234567890', 'asdasd', 'asda', 'sdsad', 'standard'),
(2, 'admin', '0c7540eb7e65b553ec1ba6b20de79608', 'Far Eastern University', 'asdasdasd', '123123', 'asd@asd', 'Vitto', 'Ivan Ralph', 'Garcia', 'male', '2017-06-01', 'asd@asd', '123123', 'asdasd', 'asdasd', 'asdasd', 'admin'),
(3, 'addasd', '79366bb2c8007ca02fdbe9a10073c16c', 'asdasd', 'asdasd', '1234567890', 'asd@asd', 'asdasd', 'asdasd', 'asdasd', 'male', '2017-06-05', 'asd@asd', '1234567890', 'asdasdasd', 'asdasdasd', 'asdasd', 'standard'),
(4, 'qwertyuiop', '540c3c6eb78fa33779eb045d5b02cd0b', 'asdkljsadlkj', 'asdkjasldkj', '1234567890', 'asd@asd', 'asdlkjasldkj', 'asldkjasdlkj', 'asldkjasdlkj', 'male', '2017-01-01', 'asd@asd', '1234567890', 'askldj', 'asdlkj', 'asdlkj', 'standard');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_entries`
--

CREATE TABLE `tbl_entries` (
  `entry_id` int(10) NOT NULL,
  `account_id` int(10) NOT NULL,
  `category` varchar(15) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `college` varchar(50) NOT NULL,
  `address` longtext NOT NULL,
  `college_dean` varchar(30) NOT NULL,
  `coach` varchar(30) NOT NULL,
  `contact_person` varchar(30) NOT NULL,
  `landline` varchar(10) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `chamberName` varchar(30) DEFAULT NULL,
  `attachPresentation` varchar(50) NOT NULL,
  `attachDocu` varchar(50) NOT NULL,
  `attachPayment` varchar(50) DEFAULT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_entries`
--

INSERT INTO `tbl_entries` (`entry_id`, `account_id`, `category`, `title`, `description`, `college`, `address`, `college_dean`, `coach`, `contact_person`, `landline`, `phone`, `email`, `chamberName`, `attachPresentation`, `attachDocu`, `attachPayment`, `status`) VALUES
(1, 1, 'Technology', 'SAKLOLO', 'asdasd', 'FAR EASTERN UNIVERSITY', 'dasd', 'asdasdasd', 'asdasdasdasd', 'asdasd', '123123', '123', 'sad', '', '1828613280.txt', '6836242671.txt', '', 'verified'),
(6, 1, 'Food', 'asdasd', 'asdasd', 'asdas', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1234567890', '1234567890', 'asd@asd', '', '7549133300.zip', '7716674801.zip', '', 'verified'),
(7, 1, 'Food', 'NANAY', 'dasdasdsad', 'MAPUA', 'dasd', 'asdasd', 'asdasdasd', 'asdasd', '1234567890', '1234567890', 'asd@asd', '', '1853942870.Victoria Sports', '5378417961.jpg', '', 'verified'),
(8, 1, 'Non-Food', 'Safety Sense', 'asdasdasd', 'CEU', 'asdas', 'dasd', 'asdasdasd', 'asdasd', '12312', '3123', 'asdasd', '', '9915161130.jpg', '457458491.PNG', '', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `student_id` int(10) NOT NULL,
  `entry_id` int(10) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `birthdate` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `contact_person` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`student_id`, `entry_id`, `firstname`, `lastname`, `email`, `mobile`, `birthdate`, `gender`, `contact_person`) VALUES
(1, 1, 'asdsa', 'dsadas', '', '', '2017-05-31', 'Male', 1),
(2, 1, 'asdas', 'dasdas', '', '', '2017-05-31', 'Male', 1),
(3, 0, 'asdas', 'dasdasd', '', '', '2017-05-30', 'Male', 1),
(4, 0, 'asdasd', 'asdasd', '', '', '2017-06-14', 'Female', 1),
(5, 0, 'asdas', 'dasdasd', '', '', '2017-05-30', 'Male', 1),
(6, 0, 'asdasd', 'asdasd', '', '', '2017-06-14', 'Female', 1),
(7, 0, 'asdas', 'dasdasd', '', '', '2017-05-30', 'Male', 1),
(8, 0, 'asdasd', 'asdasd', '', '', '2017-06-14', 'Female', 1),
(9, 1, 'asdas', 'dasdasd', '', '', '2017-05-30', 'Male', 1),
(10, 1, 'asdasd', 'asdasd', '', '', '2017-06-14', 'Female', 1),
(11, 7, 'asdasd', 'asd', '', '', '2017-05-31', 'Male', 1),
(12, 7, 'asdasd', 'asdas', '', '', '2017-06-08', 'Female', 1),
(13, 8, 'asdasdasd', 'asdas', 'asdasd', '213123', '2017-06-29', 'Female', 1),
(14, 8, 'asdas', 'dasd', 'asdasdasd', '123213', '2017-07-11', 'Male', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_accounts`
--
ALTER TABLE `tbl_accounts`
  ADD PRIMARY KEY (`account_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `tbl_entries`
--
ALTER TABLE `tbl_entries`
  ADD PRIMARY KEY (`entry_id`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_accounts`
--
ALTER TABLE `tbl_accounts`
  MODIFY `account_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_entries`
--
ALTER TABLE `tbl_entries`
  MODIFY `entry_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_students`
--
ALTER TABLE `tbl_students`
  MODIFY `student_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
